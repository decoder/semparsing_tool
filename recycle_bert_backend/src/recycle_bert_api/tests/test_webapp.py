#!/usr/bin/env python3
# coding: utf-8
from chorali_api.webapp.main import (read_storage,
                                     upload_document,
                                     ocr_document,
                                     semantize_ocr_data,
                                     post_process_ocr_data,
                                     reformat_data_to_excel)
from chorali_api.webapp.schemas.semantic_task import (UploadResult,
                                                      PostProcessTask,
                                                      ReformatTask)
from chorali_api.webapp.schemas.ocr_task import OcrTask
import asyncio
import os
import argparse
import json
import shutil
import time

from fastapi.encoders import jsonable_encoder
from fastapi import UploadFile

os.environ["LOGLEVEL"] = "INFO"

# from chorali_api.webapp.schemas.ocr_data_extract import *
# from chorali_api.webapp.schemas.ocr_serialize import *
# from chorali_api.webapp.schemas.summary import *


def getFileSize(fileobject):
    fileobject.seek(0, 2)
    # move the cursor to the end of the file
    size = fileobject.tell()
    return size


def analyze_cluster(input_file, webapp_host, with_webhook=False):

    #print("Upload document : input_file={}".format(input_file.name))
    #try:
        #fin = open(input_file.name, 'rb')
        #file_size = getFileSize(fin)
        #uploadFile = UploadFile(filename=input_file.name, file=fin)
        #asyncio.run(uploadFile.seek(0))
    #except IOError:
        #print("Error : could not read input_file {}".format(input_file.name))
        #return False
    ## asyncio.run( uploadFile.write(input_file.read()) )
    ## asyncio.run( uploadFile.seek(0) )
    #uploadRes_data = asyncio.run(upload_document(uploadFile, file_size))
    #uploadRes = UploadResult.parse_raw(uploadRes_data.body)
    #print(" => {}".format(uploadRes))
    #fin.close()

    #task_name = uploadRes.task_name
    #if not with_webhook:
        #print("Request OCR : task_name={}, no webhook".format(task_name))
        #ocrTask = OcrTask.construct(task_name=task_name, webhook_name=None)
        #ocrTaskResult = asyncio.run(ocr_document(ocrTask))
        #print(" => status_ok = {}".format(ocrTaskResult.status_ok))
        #print("    output_file     = {}".format(ocrTaskResult.output_file))
        #print("ocr results :")
        #ocrDataExtract = ocrTaskResult.ocrDataExtract
        #print(" => reportNumber = {}".format(ocrDataExtract.reportNumber))
        #print("    customerName = {}".format(ocrDataExtract.customerName))
        #print("    reportDate   = {}".format(ocrDataExtract.reportDate))
        #print("    fileName     = {}".format(ocrDataExtract.fileName))
        #print("    summary : got {} customer labels".format(
            #len(ocrDataExtract.summary)))
        #print("    highLevelResults : got {} test results".format(
            #len(ocrDataExtract.highLevelResults)))
        #print("    analyteLevelResults : got {} results".format(
            #len(ocrDataExtract.analyteLevelResults)))

    #else:
        #webhook_name = "save_result"
        #print("Request OCR : task_name={}, webhook_name={}".format(
            #task_name, webhook_name))
        #ocrTask = OcrTask.construct(task_name=task_name,
                                    #webhook_name=webhook_name)
        #ocrTaskResult = asyncio.run(ocr_document(ocrTask))
        #print(" => status_ok = {}".format(ocrTaskResult.status_ok))
        #print("    output_file     = {}".format(ocrTaskResult.output_file))
        #print(" Wait several seconds")
        #time.spleep(10)
        #print("Request ocr results")
        #fileResponse = asyncio.run(read_storage(ocrTaskResult.output_file))
        #raw_data = json.load(open(fileResponse.path))
        #ocrDataExtract = ocrDataExtract.parse_obj(raw_data)
        #print(" => reportNumber = {}".format(ocrDataExtract.reportNumber))
        #print("    customerName = {}".format(ocrDataExtract.customerName))
        #print("    reportDate   = {}".format(ocrDataExtract.reportDate))
        #print("    fileName     = {}".format(ocrDataExtract.fileName))
        #print("    summary : got {} customer labels".format(
            #len(ocrDataExtract.summary)))
        #print("    highLevelResults : got {} section results".format(
            #len(ocrDataExtract.highLevelResults)))
        #print("    analyteLevelResults : got {} sub_section results".format(
            #len(ocrDataExtract.analyteLevelResults)))

    #print("Request label alignement: task_name={}".format(task_name))
    #ocrDataExtract = asyncio.run(semantize_ocr_data(ocrDataExtract))
    #print(" => reportNumber = {}".format(ocrDataExtract.reportNumber))
    #print("    customerName = {}".format(ocrDataExtract.customerName))
    #print("    reportDate   = {}".format(ocrDataExtract.reportDate))
    #print("    fileName     = {}".format(ocrDataExtract.fileName))
    #print("    summary : got {} KPI".format(len(ocrDataExtract.summary)))
    #print("    highLevelResults : got {} section results".format(
        #len(ocrDataExtract.highLevelResults)))
    #print("    analyteLevelResults : got {} sub_section results".format(
        #len(ocrDataExtract.analyteLevelResults)))

    #print("Post-process: task_name={}".format(task_name))
    #ocrDataExtract = asyncio.run(post_process_ocr_data(ocrDataExtract))
    #print(" => reportNumber = {}".format(ocrDataExtract.reportNumber))
    #print("    customerName = {}".format(ocrDataExtract.customerName))
    #print("    reportDate   = {}".format(ocrDataExtract.reportDate))
    #print("    fileName     = {}".format(ocrDataExtract.fileName))
    #print("    summary : got {} KPI".format(len(ocrDataExtract.summary)))
    #print("    highLevelResults : got {} section results".format(
        #len(ocrDataExtract.highLevelResults)))
    #print("    analyteLevelResults : got {} sub_section results".format(
        #len(ocrDataExtract.analyteLevelResults)))

    #output_file = input_file.name + ".json"
    #print("Write Final result output_file = {}".format(output_file))
    #fout = open(output_file, 'w')
    #json_compatible = jsonable_encoder(ocrDataExtract)
    #json.dump(json_compatible, fout, indent=4)
    #fout.close()

    #print("Reformat data to Excel: task_name={}".format(task_name))
    #reformatTask = asyncio.run(reformat_data_to_excel(ocrDataExtract))
    #print(" => output_file = {}".format(reformatTask.output_file))
    #fileResponse = asyncio.run(read_storage(reformatTask.output_file))
    #output_excel_file = input_file.name + ".xlsx"
    #print("Write Final result output_excel_file = {}".format(
        #output_excel_file))
    #shutil.copy(fileResponse.path, output_excel_file)

    return True


def main():
    parser = argparse.ArgumentParser(
        description="Testing the webapp")
    parser.add_argument("input_files", type=argparse.FileType('rb'),
                        help="list of input cluster documents", nargs='+')
    parser.add_argument("-v", "--verbose", type=int, default=0, help="verbose")
    args = parser.parse_args()

    webapp_host = "127.0.0.1:8000"
    with_webhook = False

    for i, input_file in enumerate(args.input_files):
        print("==========\n{} - {}".format(i, input_file.name))
        res = analyze_cluster(input_file, webapp_host, with_webhook)
        print(" done")


if __name__ == "__main__":
    main()
