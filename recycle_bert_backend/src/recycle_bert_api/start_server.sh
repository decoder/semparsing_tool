#!/bin/bash
set -e
set -u


#-------------------------------------------------------------------------------
# to ensure the script actually runs inside its own folder
MY_PATH="`dirname \"$0\"`"              # relative
MY_PATH="`( cd \"$MY_PATH\" && pwd )`"  # absolutized and normalized
if [ -z "$MY_PATH" ] ; then
  # Error. for some reason, the path is not accessible
  # to the script (e.g. permissions re-evaled after suid)
  exit 1 # Fail
fi
cd $MY_PATH

export PYTHONPATH=$MY_PATH/../

#export APP_SETTINGS_MODULE=webapp.settings.production
export APP_SETTINGS_MODULE=webapp.settings.local
export PYTHONUNBUFFERED=1

uvicorn webapp.main:app --reload
