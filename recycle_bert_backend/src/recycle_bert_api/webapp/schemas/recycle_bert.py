import datetime
import sys
from pydantic import (BaseModel, validator)
from typing import (Optional,
                    List,
                    Union,
                    Dict)


class RecycleBertConfiguration(BaseModel):
    pass

class RecycleBert(BaseModel):
    text: str

