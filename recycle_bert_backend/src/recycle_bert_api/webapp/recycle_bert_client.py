#!/usr/bin/env python3
# coding: utf-8

import fairseq
import os
import sys

from txt2code import Txt2Code
from logging import getLogger
from typing import List

app_logger = getLogger("webapp")

class RecycleBertClientException(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return 'RecycleBertClientException, {0} '.format(self.message)
        else:
            return 'RecycleBertClientException has been raised'


class RecycleBertClient:
    def __init__(self, args=[]):
        app_logger.info(f"RecycleBertClient.__init__ bépo")
        print(f"RecycleBertClient.__init__ auie", file=sys.stderr)
        sys.argv.append(f"--user-dir={os.environ.get('CODE', '/app/acsl/user_code')}")
        parser = fairseq.options.get_generation_parser(interactive=True)
        print(f"RecycleBertClient.__init__ {os.environ.get('CODE', '/app/acsl/user_code')}",
              file=sys.stderr)
        input_args = args + [
                f"--user-dir={os.environ.get('CODE', '/app/acsl/user_code')}",
                f"-s={os.environ.get('SRC', 'src')}",
                f"-t={os.environ.get('TGT', 'tgt')}",
                "--cpu",
                "--task=translation_with_bert",
                f"--bert-model={os.environ.get('BERT_MODEL', '/app/models/acsl/mymodel')}",
                "--no-progress-bar",
                "--gen-subset=test",
                "--max-len-b=200",
                "--batch-size=32",
                "--no-repeat-ngram-size=0",
                (f"--path={os.environ.get('MODEL_STAGE2', '/app/models/acsl/model.stage2')}/checkpoint_best.pt"),
                f"--lenpen={os.environ.get('PENALTY', '1')}",
                "--sampling", "--sampling-topk=30", "--sampling-topp=0.8",
                "--temperature=0.5",
                "--beam=1", "--nbest=1",
                f"{os.environ.get('MODEL_DATA', '/app/models/acsl')}",
                "--tokenizer=space", "--bpe=bert", "--remove-bpe",
                f"--bpe-vocab-file={os.environ.get('BERT_TOKENIZER')}/vocab.txt",
                # f"--beam={os.environ.get('BEAM', '10')}",
                # "--skip-invalid-size-inputs-valid-test",
                # f"--no_repeat_ngram_size={os.environ.get('NO_REPEAT_NGRAM_SIZE', '3')}",
                ]

        print(f"RecycleBertClient.__init__ input_args: {input_args}", file=sys.stderr)
        self.args = fairseq.options.parse_args_and_arch(parser, parse_known=True,
                                                        input_args=input_args)[0]
        app_logger.info(f"RecycleBertClient.__init__ args: {self.args}")
        print(f"RecycleBertClient init args: {self.args}", file=sys.stderr)
        self.translator = Txt2Code(self.args)

    def translate(self, text: str) -> str:
        # configuration: ReformerConfiguration()):
        print(f"RecycleBertClient.translate {text}", file=sys.stderr)
        ret = self.translator.translate(text)
        result = ret[0][2]
        result = result.replace("[CLS]", "").replace("[SEP]", "").strip()
        print(f"RecycleBertClient.translate result is: {result}", file=sys.stderr)
        return result
