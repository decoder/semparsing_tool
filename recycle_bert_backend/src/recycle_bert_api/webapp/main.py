#!/usr/bin/env python3
# coding: utf-8

import json
import uvicorn

from logging import getLogger
from fastapi import (FastAPI)

from schemas.recycle_bert import RecycleBert
from recycle_bert_client import RecycleBertClient


app_logger = getLogger("webapp")

app = FastAPI()
app.state.recycle_bert_client = RecycleBertClient()

@app.post("/translate/")
def translate(data: RecycleBert):
    app_logger.info(f"/translate {data.text}")
    global recycle_bert_client
    # Extract texts from cluster
    # Call service. Return value
    result = app.state.recycle_bert_client.translate(data.text)
    #result = ret[0][2]
    #result = result.replace("[CLS]", "").replace("[SEP]", "").strip()
    app_logger.debug(f"/translate result is `{result}`")
    return json.dumps(result)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)

