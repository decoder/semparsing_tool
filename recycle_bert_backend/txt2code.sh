#! /bin/bash
#
# Interactive translation
#

trap 'exit 2' 2

SETTINGS="${1:-settings.sh}"

set -o nounset
set -o errexit
set -o pipefail

source "${SETTINGS}"

MODEL=${DATA}/model.stage2
BEAM=10
PENALTY=1.0
GPUID=""

#
# Usage
#
usage_exit() {
    echo "Usage $0 [-g GPUIDs]" 1>&2
    exit 1
}

#
# Options
#
#while getopts s:t:g:c:h OPT; do
while getopts s:t:g:h OPT; do
    case $OPT in
    s)
        SRC=$OPTARG
        ;;
    t)
        TGT=$OPTARG
        ;;
    g)
        GPUID=$OPTARG
        ;;
    h)
        usage_exit
        ;;
    \?)
        usage_exit
        ;;
    esac
done
shift $((OPTIND - 1))

if [ -n "$GPUID" ]; then
    export CUDA_VISIBLE_DEVICES=$GPUID
fi

### Translation
python ./txt2code.py "$DATA" \
    -s $SRC -t $TGT \
    --user-dir $CODE \
    --task translation_with_bert \
    --bert-model $BERT_MODEL \
    --no-progress-bar \
    --gen-subset test \
    --path $MODEL_STAGE2/checkpoint_best.pt \
    --max-len-b 200 \
    --lenpen $PENALTY \
    --batch-size 32 \
    --no-repeat-ngram-size 0 \
    --sampling --sampling-topk 30 --sampling-topp 0.8 --temperature 0.5 \
    --beam 1 --nbest 1 \
    --tokenizer space \
    --bpe bert \
    --remove-bpe \
    --bpe-vocab-file ${BERT_TOKENIZER}/vocab.txt


#     --srcdict $DATA/$CORPUS_NAME.dict.$SRC.txt \
#     --tgtdict $DATA/$CORPUS_NAME.dict.$TGT.txt \
#     --beam "$BEAM" \
#     --bpe-vocab-file ${DATA}/cpp-1000000.train.spm.src.vocab \


