#!/usr/bin/env python3 -u
# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.
"""
Translate raw text with a trained model. Batches data on-the-fly.
"""

# import ast
import fileinput
import sys
from collections import namedtuple

import torch
from fairseq import checkpoint_utils, options, tasks, utils
from fairseq.data import encoders
from typing import (List, Tuple)

Batch = namedtuple("Batch", "ids src_tokens src_lengths")
Translation = namedtuple("Translation", "src_str hypos pos_scores alignments")


def buffered_read(input, buffer_size):
    buffer = []
    with fileinput.input(files=[input], openhook=fileinput.hook_encoded("utf-8")) as h:
        for src_str in h:
            buffer.append(src_str.strip())
            if len(buffer) >= buffer_size:
                yield buffer
                buffer = []

    if len(buffer) > 0:
        yield buffer


def make_batches(lines, args, task, max_positions, encode_fn):
    tokens = [
        task.source_dictionary.encode_line(
            encode_fn(src_str), add_if_not_exist=False
        ).long()
        for src_str in lines
    ]

    lengths = torch.LongTensor([t.numel() for t in tokens])
    itr = task.get_batch_iterator(
        dataset=task.build_dataset_for_inference(tokens, lengths),
        max_tokens=200,
        max_sentences=1,
        max_positions=max_positions,
    ).next_epoch_itr(shuffle=False)
    for batch in itr:
        yield Batch(
            ids=batch["id"],
            src_tokens=batch["net_input"]["src_tokens"],
            src_lengths=batch["net_input"]["src_lengths"],
        )


class Txt2Code:
    def __init__(self, args) -> None:
        utils.import_user_module(args)

        if args.buffer_size < 1:
            args.buffer_size = 1

        assert (
            not args.sampling or args.nbest == args.beam
        ), "--sampling requires --nbest to be equal to --beam"

        #print(args, file=sys.stderr)

        self.use_cuda = torch.cuda.is_available() and not args.cpu

        # Setup task, e.g., translation
        self.task = tasks.setup_task(args)

        # Load ensemble
        print("| loading model(s) from {}".format(args.path), file=sys.stderr)
        self.models, _model_args = checkpoint_utils.load_model_ensemble(
            args.path.split(":"),
            arg_overrides=eval(args.model_overrides),
            task=self.task,
        )

        # Set dictionaries
        self.src_dict = self.task.source_dictionary
        self.tgt_dict = self.task.target_dictionary

        # Optimize ensemble for generation
        for model in self.models:
            model.make_generation_fast_(
                beamable_mm_beam_size=None if args.no_beamable_mm else args.beam,
                need_attn=args.print_alignment,
            )
            if args.fp16:
                model.half()
            if self.use_cuda:
                model.cuda()

        # Initialize generator
        self.generator = self.task.build_generator(args)

        # Handle tokenization and BPE
        self.tokenizer = encoders.build_tokenizer(args)
        #self.tokenizer = BertTokenizerFast.from_pretrained(
        #os.path.join(os.environ["BERT_TOKENIZER"])
    #)

        self.bpe = encoders.build_bpe(args)

        # Load alignment dictionary for unknown word replacement
        # (None if no unknown word replacement, empty if no path to align dictionary)
        self.align_dict = utils.load_align_dict(args.replace_unk)

        self.max_positions = utils.resolve_max_positions(
            self.task.max_positions(), *[model.max_positions() for model in self.models]
        )

        self.args = args

    def encode_fn(self, x):
        if self.tokenizer is not None:
            x = self.tokenizer.encode(x)
        if self.bpe is not None:
            x = self.bpe.encode(x)
        x = '[CLS] ' + x + ' [SEP]'
        return x

    def decode_fn(self, x):
        if self.bpe is not None:
            x = self.bpe.decode(x)
        if self.tokenizer is not None:
            x = self.tokenizer.decode(x)
        return x

    def translate(self, input) -> List[Tuple[int, tuple, str]]:
        #print("Txt2Code.translate input", file=sys.stderr)
        start_id = 0
        results = []
        str_results = []
        for batch in make_batches(
            [input], self.args, self.task, self.max_positions, self.encode_fn
        ):
            src_tokens = batch.src_tokens
            src_lengths = batch.src_lengths
            if self.use_cuda:
                src_tokens = src_tokens.cuda()
                src_lengths = src_lengths.cuda()

            sample = {
                "net_input": {
                    "src_tokens": src_tokens,
                    "src_lengths": src_lengths,
                },
            }
            translations = self.task.inference_step(self.generator, self.models, sample)
            for i, (id, hypos) in enumerate(zip(batch.ids.tolist(), translations)):
                src_tokens_i = utils.strip_pad(src_tokens[i], self.tgt_dict.pad())
                results.append((start_id + id, src_tokens_i, hypos))

        # sort output to match input order
        for id, src_tokens, hypos in sorted(results, key=lambda x: x[0]):
            if self.src_dict is not None:
                src_str = self.src_dict.string(src_tokens, self.args.remove_bpe)
                print("S-{}\t{}".format(id, src_str), file=sys.stderr)
            result = []
            # Process top predictions
            for hypo in hypos[: min(len(hypos), self.args.nbest)]:
                hypo_tokens, hypo_str, alignment = utils.post_process_prediction(
                    hypo_tokens=hypo["tokens"].int().cpu(),
                    src_str=src_str,
                    alignment=hypo["alignment"],
                    align_dict=self.align_dict,
                    tgt_dict=self.tgt_dict,
                    remove_bpe=self.args.remove_bpe,
                )
                hypo_str = self.decode_fn(hypo_str)
                #str_results.append(hypo_str)
                result.append((id, hypo["score"], hypo_str))
                print(f"H-{id}\t{hypo['score']}\t{hypo_str}", file=sys.stderr)
                #print(
                    #"P-{}\t{}".format(
                        #id,
                        #" ".join(
                            #map(
                                #lambda x: "{:.4f}".format(x),
                                #hypo["positional_scores"].tolist(),
                            #)
                        #),
                    #),
                    #file=sys.stderr,
                #)
                #if self.args.print_alignment:
                    #alignment_str = " ".join(
                        #["{}-{}".format(src, tgt) for src, tgt in alignment]
                    #)
                    #print("A-{}\t{}".format(id, alignment_str), file=sys.stderr)
            return result
        #print(f"results: {str_results}", file=sys.stderr)
        #return "\n".join(str_results)
        return []

if __name__ == "__main__":
    input = (
        "[CLS] actually walks the bag to make sure the count is correct and "
        "reset ##s the running total concode_field_sep object _ current "
        "concode_elem_sep int _ total concode_elem_sep default ##ma ##p ##bag _ "
        "parent concode_elem_sep map _ map concode_elem_sep int _ mod ##s "
        "concode_elem_sep it ##era ##tor _ support concode_field_sep boo ##lean "
        "add concode_elem_sep boo ##lean add concode_elem_sep object next "
        "concode_elem_sep boo ##lean contains ##all concode_elem_sep boo ##lean "
        "contains ##all concode_elem_sep void clear concode_elem_sep boo ##lean "
        "is ##em ##pt ##y concode_elem_sep boo ##lean hasn ##ex ##t "
        "concode_elem_sep void remove concode_elem_sep boo ##lean remove "
        "concode_elem_sep boo ##lean remove concode_elem_sep map get ##ma ##p "
        "concode_elem_sep int mod ##co ##unt concode_elem_sep boo ##lean "
        "contains concode_elem_sep it ##era ##tor it ##era ##tor "
        "concode_elem_sep boo ##lean remove ##all concode_elem_sep int size "
        "concode_elem_sep boo ##lean add ##all concode_elem_sep int hash ##code "
        "concode_elem_sep boo ##lean equals concode_elem_sep object [ ] to ##ar "
        "##ray concode_elem_sep object [ ] to ##ar ##ray concode_elem_sep set "
        "unique ##set concode_elem_sep void set ##ma ##p concode_elem_sep "
        "string to ##st ##ring concode_elem_sep int get ##co ##unt "
        "concode_elem_sep list extract ##list concode_elem_sep boo ##lean "
        "retain ##all concode_elem_sep boo ##lean retain ##all [SEP]"
    )
    parser = options.get_generation_parser(interactive=True)
    args = options.parse_args_and_arch(parser)
    #print(f"args: {args}", file=sys.stderr)
    translator = Txt2Code(args)
    #print(translator.translate(input))
    translator.translate("behaviors are complete and disjoint")
    translator.translate("a points to a block of at least n elements, with read access")
    translator.translate("Acquire a handle to the protected object. As a side effect, the protected object will be locked from access by any other thread. The lock will be automatically released when the handle is destroyed.")
    translator.translate("Returns the rank of this process in the world.")

    #print("""
        #From eval:
        #S-34593 [CLS] returns the rank of this process in the world . [SEP]
        #T-34593 [CLS] inline int rank ( ) con ##st { return rank _ ; } [SEP]
        #H-34593 -0.08268547803163528    [CLS] int rank ( ) con ##st { return _ rank ; } [SEP]
          #""")
