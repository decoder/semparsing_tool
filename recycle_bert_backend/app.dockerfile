FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

RUN apt-get update && apt-get install -y mcl -qq

COPY ./requirements.txt /
RUN pip3 install -r /requirements.txt

COPY ./fairseq /fairseq
WORKDIR /fairseq
RUN pip install --editable ./

RUN python -m spacy download en_core_web_sm
RUN python -m spacy download fr_core_news_sm

WORKDIR /

# ========= PREPARE RUNTIME ENVIRONEMENT
ARG PUID
ARG GUID

# CREATE APP USER
RUN groupadd -g ${GUID} appuser && \
    useradd -r -u ${PUID} -g appuser appuser

RUN chown -R ${PUID}:${GUID} /app

RUN install -d /home/appuser
RUN chown -R ${PUID}:${GUID} /home/appuser

RUN install -d /instance
RUN chown -R ${PUID}:${GUID} /instance

RUN install -d /storage
RUN chown -R ${PUID}:${GUID} /storage

RUN install -d /data
RUN chown -R ${PUID}:${GUID} /data

COPY ./src/reformer_api/webapp /app

USER appuser
#RUN install -d /data/input
#RUN install -d /data/output
#RUN install -d /home/appuser/.local/share/lima/resources/TensorFlowLemmatizer/ud
#RUN install -d /home/appuser/.local/share/lima/resources/TensorFlowTokenizer/ud
#RUN install -d /home/appuser/.local/share/lima/resources/TensorFlowMorphoSyntax/ud
#RUN python -c "from aymara import lima_models ; lima_models.install_language('eng')"
#RUN python -c "from aymara import lima_models ; lima_models.install_language('fra')"
