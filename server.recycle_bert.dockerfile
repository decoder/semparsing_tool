FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

RUN install -d /app/models

RUN pip install --upgrade pip

RUN ls /
COPY recycle_bert_backend/fairseq /fairseq
RUN ls /fairseq
WORKDIR /fairseq
RUN pip install --editable ./

COPY recycle_bert_backend/transformer_NMT/recycle_bert/requirements.txt /app
RUN pip install -r /app/requirements.txt


COPY recycle_bert_backend/transformer_NMT/recycle_bert/acsl /app/acsl
COPY recycle_bert_backend/transformer_NMT/recycle_bert/cpp_dir /app/cpp_dir
COPY recycle_bert_backend/txt2code.py /app

EXPOSE 1028

#ARG PUID
#ARG GUID

# CREATE APP USER
#RUN groupadd -g ${GUID} appuser && \
#    useradd -r -u ${PUID} -g appuser appuser

#RUN chown -R ${PUID}:${GUID} /app

RUN install -d /home/appuser
#RUN chown -R ${PUID}:${GUID} /home/appuser

RUN install -d /instance
#RUN chown -R ${PUID}:${GUID} /instance

RUN install -d /storage
#RUN chown -R ${PUID}:${GUID} /storage

RUN install -d /data
#RUN chown -R ${PUID}:${GUID} /data


COPY recycle_bert_backend/src/recycle_bert_api/webapp /app

WORKDIR /app

#USER appuser
