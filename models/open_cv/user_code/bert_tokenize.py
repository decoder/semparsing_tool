#!/usr/bin/env /home/yannis/miniconda3/envs/bert_env/bin/python
# -*- Coding: utf-8; -*-
#
# Copyright (c) 2019 National Institute of Information and Communications Technology.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.
#
"""
Tokenize raw sentences with the BERT tokenizer.
"""

import argparse
import ast
import sys

from transformers import BertTokenizerFast


def main(args):
    tokenizer = BertTokenizerFast.from_pretrained(
        "/scratch_global/yannis/NMT/data_concode_only_nl/mytokenizer"
    )

    for line in sys.stdin:
        line_eval = line
        tokens = tokenizer.tokenize(line_eval)
        print(" ".join(["[CLS]"] + tokens + ["[SEP]"]))


def cli_main():
    parser = argparse.ArgumentParser(
        description="Tokenize raw sentences with the BERT tokenizer"
    )
    parser.add_argument(
        "-m",
        "--model",
        type=str,
        metavar="DIR",
        dest="bert_model",
        default="bert-base-uncased",
        help="path to the BERT model",
    )
    parser.add_argument(
        "--spec_toks",
        type=list,
        dest="additional_spec_tokens",
        default=["concode_field_sep", "concode_elem_sep", "concode_func_sep"],
    )
    args = parser.parse_args()
    main(args)


if __name__ == "__main__":
    cli_main()
