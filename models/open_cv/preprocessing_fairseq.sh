#! /bin/bash

source /home/yannis/miniconda3/bin/activate
source activate bert_env

DIR=$(cd $(dirname $0);pwd)
CODE=$DIR/user_code
#CORPUS=$DIR/corpus
DATA=/scratch_global/yannis/NMT/data_opencv
SRC=src
TRG=tgt

### Convert the BERT vocabulary into fairseq one.
cat $DATA/mytokenizer/vocab.txt \
    | tail -n +5 \
    | sed -e 's/$/ 0/' \
	  > $DATA/dict.$SRC.txt
#add 3 special tokens in dict.src (TODO) 


### Encode corpora into binary sets.
$CODE/preprocess.py \
    --workers 4 \
    --source-lang $SRC --target-lang $TRG \
    --srcdict $DATA/dict.$SRC.txt \
    --tgtdict $DATA/dict.$TRG.txt \
    --trainpref $DATA/train.bpe \
    --destdir $DATA \
    --validpref $DATA/valid.bpe \
    --testpref $DATA/test.bpe,$DATA/test.bpe \
