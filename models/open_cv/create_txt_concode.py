import argparse
import ast
import io
import math
import os
import random
import sys
from collections import Counter

import numpy as np
import pandas as pd
import spacy
import torch
from transformers import BertModel, BertTokenizerFast

parser = argparse.ArgumentParser()
parser.add_argument("data_folder", type=str)

args = parser.parse_args()

df_train = pd.read_csv(
    "/scratch_global/yannis/NMT/" + args.data_folder + "/concode_train.csv"
)
df_valid = pd.read_csv(
    "/scratch_global/yannis/NMT/" + args.data_folder + "/concode_valid.csv"
)
df_test = pd.read_csv(
    "/scratch_global/yannis/NMT/" + args.data_folder + "/concode_test.csv"
)

tokenizer = BertTokenizerFast.from_pretrained(
    "/scratch_global/yannis/NMT/data_concode_only_nl/mytokenizer"
)

df_train = df_train.loc[df_train.src.apply(lambda x: len(tokenizer.tokenize(x))) < 510]
df_train = df_train.loc[df_train.tgt.apply(lambda x: len(tokenizer.tokenize(x))) < 510]
print(len(df_train), flush=True)

df_valid = df_valid.loc[df_valid.src.apply(lambda x: len(tokenizer.tokenize(x))) < 510]
df_valid = df_valid.loc[df_valid.tgt.apply(lambda x: len(tokenizer.tokenize(x))) < 510]


df_test = df_test.loc[df_test.src.apply(lambda x: len(tokenizer.tokenize(x))) < 510]
df_test = df_test.loc[df_test.tgt.apply(lambda x: len(tokenizer.tokenize(x))) < 510]

with open("/scratch_global/yannis/NMT/" + args.data_folder + "/train.src", "w") as f:
    f.write(df_train["src"].str.cat(sep="\n"))
with open("/scratch_global/yannis/NMT/" + args.data_folder + "/train.tgt", "w") as f:
    f.write(df_train["tgt"].str.cat(sep="\n"))

with open("/scratch_global/yannis/NMT/" + args.data_folder + "/valid.src", "w") as f:
    f.write(df_valid["src"].str.cat(sep="\n"))
with open("/scratch_global/yannis/NMT/" + args.data_folder + "/valid.tgt", "w") as f:
    f.write(df_valid["tgt"].str.cat(sep="\n"))

with open("/scratch_global/yannis/NMT/" + args.data_folder + "/test.src", "w") as f:
    f.write(df_test["src"].str.cat(sep="\n"))
with open("/scratch_global/yannis/NMT/" + args.data_folder + "/test.tgt", "w") as f:
    f.write(df_test["tgt"].str.cat(sep="\n"))
