#! /bin/bash

source /home/yannis/miniconda3/bin/activate
source activate bert_env

DIR=$(cd $(dirname $0);pwd)
CODE=$DIR/user_code
CORPUS=/scratch_global/yannis/NMT/data_opencv
SRC=src
TRG=tgt

#export PYTHONPATH="$CODE:$PYTHONPATH"

# sp_encode () {
#     lang=$1
#     size=$2
#     spm_train --model_prefix=$CORPUS/train.spm.$lang \
# 	      --input=$CORPUS/train.$lang \
# 	      --vocab_size=$size \
# 	      --character_coverage=1.0 \
# 	      > $CORPUS/train.spm.$lang.log 2>&1
# }

# sp_encode   $SRC 34 &
# wait

# ### sentencepiece
# sp_decode () {
#     lang=$1
#     testset=$2
#     cat $CORPUS/${testset}.$lang \
# 	| spm_encode --model=$CORPUS/train.spm.$lang.model \
# 	> $CORPUS/${testset}.bpe.$lang
# }

# sp_decode   $SRC train

### BERT tokenizer
bert_decode () {
    lang=$1
    testset=$2
    cat $CORPUS/${testset}.$lang \
	| $CODE/bert_tokenize.py \
		    > $CORPUS/${testset}.bpe.$lang
}
bert_decode $SRC train
bert_decode $TRG train

bert_decode $SRC valid
bert_decode $TRG valid

bert_decode $SRC test
bert_decode $TRG test


