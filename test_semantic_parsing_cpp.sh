#!/bin/bash

host="localhost"
port=700
text="X and Y must be within the interval [–200.0, 200]"
target_language="cpp"
context='{ \"var_names\" : [], \"var_types\" : [], \"method_names\" : [], \"method_returns\" : []}'
project="myproject"

function encode
{
  result=$(python -c "import urllib.parse;print(urllib.parse.quote('$1'))")
  echo $result
}

# JSON='{ "nl" : ["Generate", "mappings", "for", "each", "Function", "node",          "and",          "parameters",          "and",          "variables",          "names",          "associated",          "with",          "it",          "."        ],      "var_names" : [          "parentScope",          "functionBracePositions",          "funcObjects",          "functionNum",          "functionVarMappings",          "lastTokenCount",          "replacedTokens"      ],      "var_types" : [          "int",          "ArrayList",          "ObjArray",          "int",          "ArrayList",          "int",          "ArrayList"      ],      "method_names" : [          "isInScopeChain",          "reset",          "leaveNestingLevel",          "getMappedToken",          "getPreviousTokenMapping",          "collectFuncNodes",          "sourceCompress",          "enterNestingLevel"      ],      "method_returns" : [          "boolean",          "void",          "void",          "String",          "String",          "void",          "int",          "void"      ]}'

JSON="{ \"text\" : \"${text}\", \
        \"context\" : \"${context}\", \
        \"target_language\": \"$target_language\" \
      }"
#        \"project_id\" : \"${project}\"\

echo ${JSON}
command="curl --request POST  --url http://$host:$port/semparser --header 'content-type: application/json'   --data '${JSON}'"

cat <<EOF
command:
$command

response:
$(eval ${command})
EOF
