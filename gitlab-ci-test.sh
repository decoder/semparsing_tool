#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

echo "IMAGE_TAG: $IMAGE_TAG"
echo "CI_REGISTRY_USER: $CI_REGISTRY_USER"
echo "CI_REGISTRY_PASSWORD: $(expr length x${CI_REGISTRY_PASSWORD})"
echo "CI_REGISTRY: $CI_REGISTRY"
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker pull $MONGO_IMAGE_TAG
docker pull $FRAMAC_IMAGE_TAG
docker pull $PKM_IMAGE_TAG
docker pull $PARSING_IMAGE_TAG
docker pull $IMAGE_TAG
docker run -d $MONGO_IMAGE_TAG
docker run -d $FRAMAC_IMAGE_TAG
docker run -d $PKM_IMAGE_TAG
docker run -d $PARSING_IMAGE_TAG
docker run -d $IMAGE_TAG
test_semantic_parsing_pkm.sh
