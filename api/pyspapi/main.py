#!/usr/bin/env python3

import os, sys, logging
from logging.config import dictConfig
from flask import Flask, request
from flasgger import Swagger
from flasgger import APISpec, Schema, Swagger, fields
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin
from .v1.routes import semparsing
from .v1.routes import pkmfilesemparser

import ca_bundle

ca_bundle.install()

try:
    debug = os.environ.get('DEBUG').lower() == "true"
except:
    debug = True


spec = APISpec(
    title="Decoder Semantic Parsing tool",
    version="0.0.1",
    openapi_version="3.0.2",
    info=dict(description="A Semantic Parsing tool for the Decoder project"),
    plugins=[FlaskPlugin(), MarshmallowPlugin()],
)




def _initialize_errorhandlers(application):
    '''
    Initialize error handlers
    '''
    from .errors import errors
    application.register_blueprint(errors)




def create_app():

    dictConfig({
        'version': 1,
        'formatters': {'default': {
            'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
        }},
        'handlers': {'wsgi': {
            'class': 'logging.StreamHandler',
            'stream': 'ext://flask.logging.wsgi_errors_stream',
            'formatter': 'default'
        }},
        'root': {
            'level': 'DEBUG',
            'handlers': ['wsgi']
        }
    })

    # Create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config['SECRET_KEY'] = 'b0bff1b3-32d1-4ec5-b6e6-f31af9ed682e'

    with app.app_context():
        #ensure_dir(app.instance_path)  # Ensure the instance folder exists

        ## Create temp directory where text files will be stored
        #app.config.txt_folder = os.path.join(app.instance_path, "temp_text")
        #ensure_dir(app.config.txt_folder)

        # Register home blueprint
        from . import home
        app.register_blueprint(home.bp)
        _initialize_errorhandlers(app)


        # Register api v1
        from .v1.routes import api as api_v1
        app.register_blueprint(api_v1, url_prefix="/")


    return app


app = create_app()

with app.test_request_context():
    spec.path(view=semparsing)
    spec.path(view=pkmfilesemparser)


