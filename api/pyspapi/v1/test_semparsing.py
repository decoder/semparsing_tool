#!/usr/bin/env python3

import os
import requests
import sys
from pkm_client import PKMClient

PKM_USER = os.environ.get('PKM_USER')
if PKM_USER is None:
    raise RuntimeError('PKM user name must be available in the environment '
                       'variable PKM_USER')
PKM_PASSWORD = os.environ.get('PKM_PASSWORD')
if PKM_PASSWORD is None:
    raise RuntimeError('PKM user password must be available in the '
                       'environment variable PKM_PASSWORD')
user_name = PKM_USER
user_password = PKM_PASSWORD
project = "mydb"
filename = "examples%2Fvector2.c"
certificate = "ssl/pkm_docker.crt"

pkm = PKMClient(cacert=certificate)


def exit_status(STATUS):
    if STATUS == 0:
        print("Test PASSED")
    else:
        print("Test FAILED")

    sys.exit(STATUS)


print("user's login")
status = pkm.login(user_name=user_name, user_password=user_password)
if not status:
    print("user's login failed", file=sys.stderr)
    exit_status(1)

print(f"getting comments of project {project}")
status, doc = pkm.call(method="GET",
                       path=f"code/c/comments/{project}/{filename}")
#status, doc = pkm.call(method="GET", path=f"code/c/comments/{project}")
if status != 0:
    print(f"getting comments of project {project} failed", file=sys.stderr)
    exit_status(1)

print(doc)


if not isinstance(doc, list):
    doc = [doc]

for comment in doc:
    if 'global_kind' in comment and comment['global_kind'] == 'GFun':
        comment_string = ''.join(comment['comments']).replace('\\n', '\n')
        if not 'Copyright' in comment_string:
            comment_string += "a new line there\n"
            comment['comments'] = [
              line+"\\n" for line in comment_string.rstrip().split("\n")]
    print(comment, file=sys.stderr)

print(f"writing back comments of project {project}")
status, res = pkm.call(method="PUT", path=f"code/c/comments/{project}",
                       payload=doc)
if status != 0:
    print(f"writing back comments of project {project} failed",
          file=sys.stderr)
    exit_status(1)

print("checking comments of project f{project} after changes",
      file=sys.stderr)
status, doc = pkm.call(method="GET",
                       path=f"code/c/comments/{project}/{filename}")
#status, doc = pkm.call(method="GET", path=f"code/c/comments/{project}")
if status != 0:
    print(f"checking comments of project {project} after changes failed",
          file=sys.stderr)
    exit_status(1)

print(doc)


