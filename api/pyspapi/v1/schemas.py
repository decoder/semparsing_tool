from pydantic import BaseModel
from typing import Optional


class PKMRawSourceCode(BaseModel):
    rel_path: str
    content: str
    format: str
    encoding: str
    type: str
    mime_type: str
    git_working_tree: Optional[str]
    git_dirty: Optional[bool]
    git_unmerged: Optional[bool]
