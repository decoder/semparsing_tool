from datetime import datetime as dt
import flask
from flask import Blueprint, request, jsonify, make_response, abort, request
import json
import logging
from marshmallow import Schema, fields, validate
import os
import requests
import sys
import time
import traceback
import urllib.parse

from typing import List, Optional, Tuple

from .client import parse
from .schemas import PKMRawSourceCode

from pkm_python_client.pkm_client import PKMClient
from pkm_python_client.pkm_client import Log

api = Blueprint('api_v1', __name__)

PKM_USER = os.environ.get('PKM_USER')
PKM_PASSWORD = os.environ.get('PKM_PASSWORD')
PKM_CERT = os.environ.get('PKM_CERT')
if PKM_CERT is None:
    PKM_CERT = "/ssl/pkm_docker.crt"


print(f'routes PKM_USER={PKM_USER}, PKM_PASSWORD={PKM_PASSWORD}',
      file=sys.stderr)

try:
    debug = os.getenv('DEBUG').lower() == "true"
except Exception:
    debug = True


class PKMFileSemParser(Schema):
    file = fields.Str(required=True)
    source_language = fields.Str(validate=validate.OneOf(["java", "c", "cpp"]),
                                 required=False)
    target_language = fields.Str(validate=validate.OneOf(["ast", "jml", "acsl",
                                                          "java", "acslpp",
                                                          "c", "cpp"]),
                                 required=True)
    project_id = fields.Str(required=True)
    invocationID = fields.Str(required=False)


class PKMFileSemParserResult(Schema):
    message = fields.Str(required=True)
    status = fields.Boolean(required=True)


class SemParserSchema(Schema):
    text = fields.Str(required=True)
    target_language = fields.Str(validate=validate.OneOf(["ast", "jml", "acsl",
                                                          "java", "acslpp",
                                                          "c", "cpp"]),
                                 required=True)
    context = fields.Str(required=False)


class SemParserResult(Schema):
    message = fields.Str(required=True)
    status = fields.Boolean(required=True)
    code = fields.Str(required=False)


@api.route('/semparser', methods=['POST'])
def semparsing() -> Tuple[str, int]:
    """ Semantic parsing of a Text into code. Supported target languages are
      AST, C, C++ and Java.
    ---
    post:
      parameters:
      - in: data
        schema: SemParserSchema
      responses:
        200:
          description: Translation completed.
          content:
            application/json:
              schema: SemParserResult
        400:
          description: Bad Request.
          content:
            application/json:
              schema: SemParserResult
        405:
          description: Method Not Allowed.
          content:
            application/json:
              schema: SemParserResult
        500:
          description: An unexpected error has occurred.
          content:
            application/json:
              schema: SemParserResult
    """

    app = flask.current_app

    semparser_schema = SemParserSchema()
    form_data = request.get_json()
    errors = semparser_schema.validate(form_data)

    if errors:
        if debug:
            app.logger.debug(f"/semparser Failed to validate request data "
                             f"'{form_data}' with respect to the schema. "
                             f"Error: {errors}.")
        return {
          "message": f"/semparser Failed to validate request data "
                     f"'{form_data}' with respect to the schema Error: "
                     f"{errors}.",
          "success": False}, 400

    text = form_data['text']
    context = form_data['context'] if 'context' in form_data else None
    target_language = form_data['target_language']
    status, res = parse_comment([text], context, target_language)
    print(f"/semparsing res: {status}, {res}", file=sys.stderr)
    result_schema = SemParserResult()
    loaded = result_schema.load(
        {
            "message": "Done",
            "status": True,
            "code": "".join(res)
            }
        )

    logging.info(f"Returning {result_schema.dumps(loaded)}.")
    return result_schema.dumps(loaded), 200


def send_result(message, status, pkm=None, project_id=None, invocation_id=None,
                resultsToAdd=[]):
    assert type(resultsToAdd) == list, (f"resultsToAdd ({resultsToAdd}) should be a "
                                        f"list but it is a {type(resultsToAdd)}")
    app = flask.current_app
    app.logger.debug(message)
    res = {
      "message": message,
      "status": (status < 300)}
    result_schema = PKMFileSemParserResult()
    loaded = result_schema.load(res)

    logging.info(f"Returning {result_schema.dumps(loaded)}.")
    if invocation_id is not None:
        # Put in invocationResults the paths of artifacts put in the pkm with type of
        # handled data ?
        pkm.update_invocation(
            project_id, invocation_id, 0 if status < 300 else status,
            resultsToAdd)
    return result_schema.dumps(loaded), status


@api.route('/pkmfilesemparser', methods=['POST'])
def pkmfilesemparser(invocationID=None) -> Tuple[str, int]:
    """
    Semantic parsing the comments of a file stored in the PKM.

    Supported translations should be Java comments to JML, C comments to ACSL, C++
    comments to ACSL++, but there is currently no model to generate formal languages.
    So, one can only generate Java and C++ code from the comments and compare with the
    existing code. One can also write empty functions with comments, then generate the
    code and (manually) add it in the function body.
    ---
    post:
      parameters:
      - name: key
        description: Access key to the PKM (optional, depending on server
                     configuration)
        in: header
        schema:
          type: string
      - name: invocationID
        in: path
        description: decoder invocation id
        required: false
        schema:
          type: string
      - in: data
        schema: PKMFileSemParser
      responses:
        200:
          description: Translation completed.
          content:
            application/json:
              schema: PKMFileSemParserResult
        400:
          description: The language is not taken into account.
          content:
            application/json:
              schema: PKMFileSemParserResult
        405:
          description: Method Not Allowed.
          content:
            application/json:
              schema: PKMFileSemParserResult
        500:
          description: An unexpected error has occurred.
          content:
            application/json:
              schema: PKMFileSemParserResult
    """
    try:
        app = flask.current_app

        key = None
        if 'keyParam' in request.headers:
            key = request.headers.get('keyParam')
        elif 'key' in request.headers:
            key = request.headers.get('key')

        form_data = request.get_json()
        project_id = None
        if "project_id" in form_data:
            project_id = form_data['project_id']
        invocation_id = request.args.get('invocationID', None)
        if invocation_id is None and 'invocationID' in form_data:
            invocation_id = form_data['invocationID']

        print(f"pkmfilesemparser project_id: {project_id}, invocation_id: {invocation_id}",
            file=sys.stderr)

        pkm = PKMClient(cacert=PKM_CERT, key=key)
        log = Log(pkm=pkm, project=project_id, tool="semparsing_tool",
                invocation_id=invocation_id)

        if key is None and PKM_USER and PKM_PASSWORD:
            if debug:
                flask.current_app.logger.debug("user's login")
            status = pkm.login(user_name=PKM_USER, user_password=PKM_PASSWORD)
            if not status:
                message = f"user's login to pkm failed: {status}"
                flask.current_app.logger.error(message)
                return send_result(message, 500, pkm, project_id, invocation_id)
        elif key is None:
            errorString = (
            "Either pkm key should be passed as http header 'key' or"
            " user/password env vars should be defined")
            flask.current_app.logger.error(errorString)
            return send_result(errorString, 400, pkm, project_id, invocation_id)

        semparser_schema = PKMFileSemParser()
        errors = semparser_schema.validate(form_data)
        if errors:
            message = (f"/pkmfilesemparser Failed to validate request "
                    f"data '{form_data}' with respect to the schema. "
                    f"Error: {errors}.")
            log.error(message)
            app.logger.error(message)
            return send_result(message, 400, pkm, project_id, invocation_id)

        file = form_data['file']
        file = urllib.parse.quote(file, safe='')

        target_language = form_data['target_language']

        source_language = "c"
        if "source_language" in form_data:
            source_language = form_data['source_language']

        if debug:
            message = (f"getting comments of project {project_id} in "
                    f"language {source_language} for file {file}")
            log.message(message)
            app.logger.debug(message)
        path = f"code/{source_language}/comments/{project_id}/{file}"
        if debug:
            message = f"pkm query path is {path}"
            log.message(message)
            app.logger.debug(message)
        status, comments = pkm.call(method="GET", path=path)
        if status != 0:
            message = f"getting comments failed {status}, {comments}"
            log.message(message)
            app.logger.error(message)
            return send_result(message, 404, pkm, project_id, invocation_id)

        if not isinstance(comments, list):
            comments = [comments]
        message = f"pkmfilesemparser comments is: {comments}"
        log.message(message)
        print(message, file=sys.stderr)

        parse_status = False
        for comment in comments:
            if debug:
                message = f"pkmfilesemparser comment: {comment}"
                log.message(message)
                app.logger.debug(message)
            if 'global_kind' in comment and comment['global_kind'] == 'GFun':
                parse_status, comment["comments"] = parse_comment(
                    comment["comments"], target_language, log)
                if debug:
                    message = f"pkmfilesemparser GFun comment[\"comments\"] after parse_comment: {comment['comments']}"
                    log.message(message)
                    app.logger.debug(message)
                if not parse_status:
                    return send_result("Error in semantic parsing backend",
                                    500, pkm, project_id, invocation_id)
            elif ('comments' in comment and isinstance(comment["comments"], list)
                and len(comment["comments"]) > 0):
                # if C, comment["comments"] is the array of strings of the comment
                if isinstance(comment["comments"][0], str):
                    parse_status, comment["comments"] = parse_comment(
                        comment["comments"], target_language, log)
                    if debug:
                        message = (f"pkmfilesemparser C comment[\"comments\"] after "
                                   f"parse_comment: {comment['comments']}")
                        log.message(message)
                        app.logger.debug(message)
                    if not parse_status:
                        return send_result("Error in semantic parsing backend",
                                           500, pkm, project_id, invocation_id)
                # if Java, comment["comments"] is an array of dict with a comments
                # member
                elif (isinstance(comment["comments"][0], dict)
                        and 'comments' in comment["comments"][0]):
                    for in_comment in comment["comments"]:
                        if ((source_language == 'c' and 'global_kind' in in_comment
                             and in_comment['global_kind'] == 'GFun')
                                or source_language != 'c'):
                            parse_status, in_comment["comments"] = parse_comment(
                                in_comment["comments"], target_language, log)
                            if debug:
                                message = (f"pkmfilesemparser {source_language} "
                                           f"comment[\"comments\"] after "
                                           f"parse_comment: {in_comment['comments']}")
                                log.message(message)
                                app.logger.debug(message)
                            if not parse_status:
                                return send_result("Error in semantic parsing backend",
                                                   500, pkm, project_id, invocation_id)
                        else:
                            # Not in a c function nor another language: will not add
                            # anything to original comment
                            in_comment["comments"] = []

            else:
                message = f"pkmfilesemparser nothing to parse"
                log.message(message)
                print(message, file=sys.stderr)

        if debug:
            message = f"writing back comments of project {project_id}: {comments}"
            log.message(message)
            app.logger.debug(message)

        # Retrieve source file from pkm
        sourcecode_url = (f"code/rawsourcecode/{project_id}/{file}")
        status, pkm_res = pkm.call("GET", sourcecode_url)
        if status != 0:
            message = (f"getting raw source code from {sourcecode_url} "
                       f"failed {status}, {pkm_res}")
            log.message(message)
            app.logger.error(message)
            return send_result(message, 404, pkm, project_id, invocation_id)
        res = PKMRawSourceCode.parse_obj(pkm_res)
        raw_source_code = res.content.splitlines()
        app.logger.debug(f"raw_source_code: {raw_source_code}")

        # Add new comments or replace the initial comments
        for comment in comments:
            if "comments" in comment:
                for inner_comment in reversed(comment["comments"]):
                    pos_end = inner_comment["loc"]["pos_end"]["pos_lnum"]
                    app.logger.debug(f"Inserting: {inner_comment['comments']}, "
                                     f"after line {pos_end}")
                    raw_source_code = (raw_source_code[:pos_end]
                                       + inner_comment["comments"]
                                       + raw_source_code[pos_end:])
        app.logger.debug(f"Updated raw_source_code: {raw_source_code}")

        res.content = "\n".join(raw_source_code)
        payload = [res.dict()]  # [PKMRawSourceCode]
        if debug:
            message = f"Payload to write back is: {payload}"
            log.message(message)
            app.logger.debug(message)
        # Write new source file to pkm
        put_sourcecode_url = (f"code/rawsourcecode/{project_id}")
        status, pkm_res = pkm.call("PUT", put_sourcecode_url, payload)
        if status != 0:
            message = (f"Writing new source code to {put_sourcecode_url} failed: "
                    f"{status}, {pkm_res}")
            log.message(message)
            app.logger.error(message)
            return send_result(message, 500, pkm, project_id, invocation_id)

        # This was updating the comment itself in the pkm but we now modify the source code
        # itself if necessary. The client is in charge to update the pkm by calling FramaC,
        # FramaCpp or JavaParser
        #put_path = f"code/{source_language}/comments/{project_id}"
        #status, res = pkm.call(
        #method="PUT",
        #path=put_path,
        #payload=doc)
        #if status != 0:
            #message = (f"Writing back comments to {put_path} failed: "
                    #f"{status}, {res}")
            #log.message(message)
            #app.logger.error(message)
            #return send_result(message, 500, pkm, project_id, invocation_id)

        return send_result("Success", 200, pkm, project_id, invocation_id,
                        [dict(path=(f"/annotations/{project_id}"
                                    # f"?path={urllib.parse.quote(put_path, safe='')}"
                                    ),
                                type="annotations")])
    except Exception as e:
        message = (f"Got an unexpected exception in pkmfilesemparser: {e}. Stackstrace: \n{traceback.format_exc()}")
        app.logger.error(message)
        log.error(message)
        return send_result(message, 500, pkm=None, project_id=None, invocation_id=None)


def call_semanticparsing_recycle_bert_backend(text: str,
                                              target_language: str) -> Tuple[int, str]:
    headers = {"accept": "application/json",
               "Content-Type": "application/json"}
    hosts = {"acsl": "semanticparsing_recycle_bert_backend",
             "cpp": "semanticparsing_recycle_bert_cpp_backend"}
    # "cpp": "semanticparsing_recycle_bert_opencv_backend"}
    try:
        url = f'http://{hosts[target_language]}/translate'
        payload = {'text': text}
        print(f"call_semanticparsing_recycle_bert_backend url: {url}, "
              f"payload: {json.dumps(payload, indent=1)[:1000]}", file=sys.stderr)

        resp = requests.request("POST", url, headers=headers, json=payload)

    except Exception as e:
        app = flask.current_app
        app.logger.error(
            f'Catch exception calling the recycle_bert service: {e}, {url}')
        return 500, f'Catch exception calling the recycle_bert service: {e}, {url}'

    http_code = resp.status_code

    print(f"call_semanticparsing_recycle_bert_backend response: {http_code}, "
          f"headers: {resp.headers}, text: {resp.text[:1000]}",
          file=sys.stderr)
    answer = ""

    if http_code < 300:
        status = 0
        if ('Content-Type' in resp.headers
                and 'application/json' in resp.headers['Content-Type']):
            answer = json.loads(resp.json())
    else:
        error_message = (f"Call to the recycle_bert service at {url} failed: "
                         f"{resp}, {resp.headers}, {resp.text}")
        print(error_message, file=sys.stderr)
        answer = error_message
        status = http_code

    return status, answer


def parse_comment(comment: List[str],
                  context: Optional[str],
                  target_language: str,
                  log: Log = None) -> Tuple[bool, List[str]]:
    app = flask.current_app
    if debug:
        app.logger.debug(f"parse_comment({comment}, {context}, {target_language})")

    # Remove traces of code summarization tool from comment
    # and other cleanups
    comment_string = (''.join(comment).replace('\\n', '\n').strip()
                      .replace("DECODER-CODE-SUMMARIZATION:", ""))
    # Ignore comments generated by self or real ACSL annotations
    if ("semanticparser:" in comment_string.lower()
            or "/*@" in comment_string.lower()):
        return True, []
    # Hard-coded filter on copyright comments
    if 'Copyright' not in comment_string:
        if target_language in ["acsl", "cpp"]:
            status, res = call_semanticparsing_recycle_bert_backend(comment_string,
                                                                    target_language)

            if debug:
                app.logger.debug(
                    f"parse_comment call_semanticparsing_recycle_bert_backend result: "
                    f"{status}, {res}")
            if status < 300:
                if debug:
                    message = f"Translation completed {status}, {res}."
                    if log is not None:
                        log.message(message)
                    app.logger.debug(message)
                #result_string = f"/* SemanticParser: {res}\n*/"
                result_string = res
                if debug:
                    message = f"parse_comment updating comment. New value is: '{result_string}'"
                    if log is not None:
                        log.message(message)
                    app.logger.debug(message)
                return True, result_string.split("\n")
            else:
                message = f"Error in semantic parsing backend: '{res}'"
                if log is not None:
                    log.message(message)
                app.logger.error(message)
                return False, [message]
        else:
            status, res = parse(itext=comment_string, target=target_language,
                                context=context)
            if status:
                if debug:
                    message = f"Translation completed {status}, {res}."
                    if log is not None:
                        log.message(message)
                    app.logger.debug(message)
                # Append the
                if target_language == "jml":
                    result_string = ""
                    if res["code"]:
                        #result_string = f"/* SemanticParser: {res['code']}\n@*/"
                        result_string = res['code']
                else:
                    result_string = res["code"]
                if debug:
                    message = f"parse_comment Result value is: '{result_string}'"
                    if log is not None:
                        log.message(message)
                    app.logger.debug(message)
                # return True and a list of stripped newline terminated strings
                return True, result_string.split("\n")
            else:
                message = f"Error in semantic parsing backend: '{res}'"
                if log is not None:
                    log.message(message)
                app.logger.error(message)
                return False, [message]
    else:
        return True, comment


@api.after_request
def after_request(response):
    """ Logging after every request. """
    app = flask.current_app

    # logger = logging.getLogger("app.access")
    if debug:
        app.logger.debug(
            "%s [%s] %s %s %s %s %s %s %s",
            request.remote_addr,
            dt.utcnow().strftime("%d/%b/%Y:%H:%M:%S.%f")[:-3],
            request.method,
            request.path,
            request.scheme,
            response.status,
            response.content_length,
            request.referrer,
            request.user_agent,
        )
    return response


