import sys
import json
import requests
from typing import Tuple, TypedDict


class SemParsingClientResult(TypedDict):
    code: str
    message: str
    status: bool


def parse(itext: str, target: str,
          context: str = None) -> Tuple[bool, SemParsingClientResult]:
    text = itext.replace("/*", "").replace("*/", "")
    try:
        headers = {"accept": "application/json",
                    "Content-Type": "application/json"}
        url = f'http://semanticparsing_backend/{target}/'
        payload = dict(text=text)
        if target == "java" and context is not None:
            context_data = json.loads(context)
            payload.update(context_data)
        resp = requests.post(url, headers=headers, json=payload)
        if resp.status_code >= 300:
            return False, {
                "code": "",
                "message": (f"Calling semantic parsing backend failed on {url} "
                            f"and {payload}. Reason: {resp.reason}."),
                "status": False}

        res = json.loads(resp.json())
        return True, SemParsingClientResult(code=res,
                                            message="Translation completed.",
                                            status=True)
    except Exception as e:
        print(f"Client.parser catched exception {e}", file=sys.stderr)
        return False, SemParsingClientResult(code="",
                                             message="Model container error.",
                                             status=False)
