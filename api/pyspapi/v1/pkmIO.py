import json
#import netifaces
import os
import requests
import sys
from datetime import datetime
from . import pkm_client
from .pkm_client import PKMClient


class SemParsingPKMClient(PKMClient):

    def __init__(self):
        super().__init__(cacert="/ssl/pkm_docker.crt")
        self.semparsing_put_url = "/v2/code/file"

    def _create_json(self, data):
        filename = f'semparser_{data["language"]}_{data["language"]}_{datetime.now().strftime("%d_%m_%Y %H_%M_%S")}.json'
        try:
            with open(filename, 'w') as outfile:
                json.dump(data, outfile)
            return filename
        except Exception as e:
            print(f"Catched exception in semparsing_put_url._create_json: {e}",
                  file=sys.stderr)
            raise

    def _removeFile(self, filename):
        try:
            os.remove(filename)
        except OSError as e:
            print(f"Catched exception in semparsing_put_url._removeFile: {e}",
                  file=sys.stderr)

    def send_semparsing(self, projectId, text, lang, res):
        print(f"SemParsingPKMClient.send_semparsing {projectId}, "
              f"{text[:100]}, {lang}, {res}",
              file=sys.stderr)
        response = None
        data = {'text': text,
                'language': lang,
                'ast': res['result']
                }

        filename = self._create_json(data)
        #if projectId is not None and filename is not None:
        if filename is not None:
            content = {"directory": f"./{filename}",
                       "fileName": filename,
                       "content": str(data),
                       "codeFormat": ["CodeAndComments"]}

            try:
                print(f'Sending data to {self.semparsing_put_url}',
                      file=sys.stderr)
                http_status, response = self.call(path=self.semparsing_put_url,
                                                  method="PUT",
                                                  payload=content)
                print(f'from PKM {http_status}, {response}',
                      file=sys.stderr)
                if response and http_status < 300:
                    return True, filename
                else:
                    print(f"Got error when talking to PKM: {response}",
                          file=sys.stderr)
                    self._removeFile(filename)
                    return False, None
            except Exception as e:
                print(f"Catched exception when talking to PKM: {e}",
                      file=sys.stderr)
                self._removeFile(filename)
                return False, None

        else:
            return False, None
