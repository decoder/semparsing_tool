import os
import traceback
from flask import Blueprint, jsonify, current_app, request
from time import strftime

errors = Blueprint('errors', __name__)

try:
    debug = os.environ.get('DEBUG').lower() == "true"
except Exception:
    debug = True


@errors.app_errorhandler(405)
def handle_method_error(error):
    app = current_app
    ts = strftime('[%Y-%b-%d %H:%M]')
    tb = traceback.format_exc()
    if debug:
        app.logger.debug(f'{ts} {request.remote_addr} {request.method} '
                         f'{request.scheme} {request.full_path} 405 '
                         f'The method is not allowed.\n{tb}')
    status_code = 405
    success = False
    response = {
        'success': success,
        'message': "The method is not allowed."
    }

    return jsonify(response), status_code


@errors.app_errorhandler(Exception)
def handle_unexpected_error(error):
    app = current_app
    ts = strftime('[%Y-%b-%d %H:%M]')
    tb = traceback.format_exc()
    if debug:
        app.logger.debug(f'{ts} {request.remote_addr} {request.method} '
                         f'{request.scheme} {request.full_path} 500 '
                         f'An unexpected error has occurred: {error}.\n{tb}')
    status_code = 500
    success = False
    response = {
        'success': success,
        'message': f'{ts} {request.remote_addr} {request.method} '
                   f'{request.scheme} {request.full_path} 500 '
                   f'An unexpected error has occurred: {error}.\n{tb}'
    }

    return jsonify(response), status_code
