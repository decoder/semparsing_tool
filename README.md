# DECODER SP tool
```bash
fairseq-interactive --user-dir=/app/acsl/user_code -s src -t tgt --task=translation_with_bert --cpu --bert-model=/app/models/acsl/mymodel --path=/app/models/acsl/model.stage2/checkpoint_best.pt /app/models/acsl

['--user-dir=/app/acsl/user_code', '-s=src', '-t=tgt', '--cpu', '--task=translation_with_bert', '--bert-model=/app/models/acsl/mymodel', '--no-progress-bar', '--gen-subset=test', '--path=/app/models/acsl/model.stage2/checkpoint_best.pt', '--lenpen=1', '--beam=10', '--skip-invalid-size-inputs-valid-test', '--no_repeat_ngram_size=3', '/app/models/acsl']
```

This is the Semantic Parsing (SP) tool for the Decoder project (WP2)

It is a Docker composition of Flask service and a NN based translation service.

To install it, you need to follow these steps:


1. Call ``build_docker.sh`` to build the Docker container.

2. Start the Docker container with ``run_docker.sh`` .

To test the service, you can use `test_semantic_parsing_jml_pkm.sh`, `test_semantic_parsing_jml.sh`, `test_semantic_parsing_pkm.sh` and `test_semantic_parsing.sh`.

## SP API

The SP tool, when interacting with the pkm, takes as parameters the information necessary to find comments in the pkm. Its return value is only a pair of a string and a code indicating the success of the operation or explaining its failure. The actual result is that the targeted comments are completed with their equivalent in the target language.

The input parameters are used to send the following query to the pkm: `code/{source_language}/comments/{project_id}/{file}`.

### Input:
  * project_id: pkm project to query
  * source_language: the programming language of the file (java/c/cpp)
  * target_language: the target programming or constraints language (jml, acsl, acsl++, ast, java, c, cpp)
  * file: the file whose comments will be parsed to code. This


### Output:
  * status: boolean indicating the success or failure of the call
  * message: "Success" if status is true and an error message otherwise

### Side effect:

If successful, the targeted comments are completed with their translation in the target language.

## Example


Query:

```json
{
  "file": "examples%2Fvector2.c",
  "source_language": "c",
  "target_language": "ast",
  "project_id": "mydb"
}
```

The result with this call is:
```json
{
  "status": true,
  "message": "Success"
}
```

### OpenAPI api

The OpenAPI api of this service can be retrieved by pointing a navigator to the
URL of the service. As of 14/01/2021, it is:

```json
{
  "components": {
    "schemas": {
      "PKMFileSemParserResult": {
        "properties": {
          "message": {
            "type": "string"
          },
          "status": {
            "type": "boolean"
          }
        },
        "required": [
          "message",
          "status"
        ],
        "type": "object"
      },
      "SemParserResult": {
        "properties": {
          "code": {
            "type": "string"
          },
          "message": {
            "type": "string"
          },
          "status": {
            "type": "boolean"
          }
        },
        "required": [
          "message",
          "status"
        ],
        "type": "object"
      }
    }
  },
  "info": {
    "description": "A Semantic Parsing tool for the Decoder project",
    "title": "Decoder Semantic Parsing tool",
    "version": "0.0.1"
  },
  "openapi": "3.0.2",
  "paths": {
    "/pkmfilesemparser": {
      "post": {
        "parameters": [
          {
            "description": "Access key to the PKM (optional, depending on server configuration)",
            "in": "header",
            "name": "keyParam",
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "data",
            "name": "project_id",
            "required": false,
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "data",
            "name": "source_language",
            "required": false,
            "schema": {
              "enum": [
                "java",
                "c",
                "cpp"
              ],
              "type": "string"
            }
          },
          {
            "in": "data",
            "name": "target_language",
            "required": true,
            "schema": {
              "enum": [
                "ast",
                "jml",
                "acsl",
                "java",
                "acslpp",
                "c",
                "cpp"
              ],
              "type": "string"
            }
          },
          {
            "in": "data",
            "name": "file",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PKMFileSemParserResult"
                }
              }
            },
            "description": "Translation completed."
          },
          "400": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PKMFileSemParserResult"
                }
              }
            },
            "description": "The language is not taken into account."
          },
          "405": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PKMFileSemParserResult"
                }
              }
            },
            "description": "Method Not Allowed."
          },
          "500": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PKMFileSemParserResult"
                }
              }
            },
            "description": "An unexpected error has occurred."
          }
        }
      }
    },
    "/semparser": {
      "post": {
        "parameters": [
          {
            "in": "data",
            "name": "project_id",
            "required": false,
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "data",
            "name": "text",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "data",
            "name": "target_language",
            "required": true,
            "schema": {
              "enum": [
                "ast",
                "jml",
                "acsl",
                "java",
                "acslpp",
                "c",
                "cpp"
              ],
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/SemParserResult"
                }
              }
            },
            "description": "Translation completed."
          },
          "400": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/SemParserResult"
                }
              }
            },
            "description": "The language is not taken into account."
          },
          "405": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/SemParserResult"
                }
              }
            },
            "description": "Method Not Allowed."
          },
          "500": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/SemParserResult"
                }
              }
            },
            "description": "An unexpected error has occurred."
          }
        }
      }
    }
  }
}
```

# Licenses

This repository, lincensed under the 2-clause BSD license (see COPYING file) includes code from several other
repositories, all under the MIT license:
- Concode: https://github.com/sriniiyer/concode
- Fairseq: https://github.com/pytorch/fairseq
- RecycleBert: https://github.com/kenji-imamura/recycle_bert

Thanks to all the authors of these repositories for sharing their work.
