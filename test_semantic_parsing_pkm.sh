#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi
. "$DIR/pkm_rest_api.sh"

PKM_USER=gael
PKM_PASSWORD=gael
protocol="http"
host="localhost"
port=700
file="examples%2Fvector2.c"
source_language="cpp"
target_language="cpp"
# project_id="mydb"
project_id="myproject"

USER1_KEY=$(login ${PKM_USER} ${PKM_PASSWORD})
echo "Login was successful: ${USER1_KEY}"

command="curl --request POST  --url ${protocol}://${host}:${port}/pkmfilesemparser -H 'accept: application/json' -H 'Content-Type: application/json' --header 'key: ${USER1_KEY}' --data '{\"file\": \"$file\", \"source_language\": \"$source_language\", \"target_language\": \"$target_language\", \"project_id\": \"$project_id\"}'"

cat <<EOF
command:
$command

response:
$(eval ${command})
EOF
