#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi
. "$DIR/pkm_rest_api.sh"

PKM_USER="mythaistar-test-gael"
PKM_PASSWORD="(v4(GR*e"
host="localhost"
port=700
file="BankingExample.java"
source_language="java"
target_language="jml"
project_id="mythaistar-test-gael"

USER1_KEY=$(login ${PKM_USER} ${PKM_PASSWORD})
echo "Login was successful"

command="curl --request POST  --url http://$host:$port/pkmfilesemparser -H 'accept: application/json' -H 'Content-Type: application/json' --header 'key: ${USER1_KEY}' --data '{\"file\": \"$file\", \"source_language\": \"$source_language\", \"target_language\": \"$target_language\", \"project_id\": \"$project_id\"}'"

cat <<EOF
command:
$command

response:
$(eval ${command})
EOF
