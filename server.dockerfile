FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

RUN pip install --upgrade pip
COPY backend/requirements.txt /backend_requirements.txt
RUN pip install -r /backend_requirements.txt

COPY backend/webapp /app
COPY backend/server /app/server

RUN install -d /app/models

WORKDIR /app
