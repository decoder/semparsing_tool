#!/bin/bash

docker-compose --env-file ./semparsing.env build

docker tag parsing_api:latest is234070.intra.cea.fr:5000/parsing_api:latest
docker tag parsing_backend:latest is234070.intra.cea.fr:5000/parsing_backend:latest
docker tag recycle_bert_backend:latest is234070.intra.cea.fr:5000/recycle_bert_backend:latest
docker tag recycle_bert_cpp_backend:latest is234070.intra.cea.fr:5000/recycle_bert_cpp_backend:latest
docker tag recycle_bert_opencv_backend:latest is234070.intra.cea.fr:5000/recycle_bert_opencv_backend:latest
