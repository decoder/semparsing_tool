import os

APP_SETTINGS_MODULE = os.environ.get("APP_SETTINGS_MODULE", "settings.production")

SETTINGS = __import__(APP_SETTINGS_MODULE, fromlist=[None])
