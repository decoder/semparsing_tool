import os
from os.path import dirname, abspath, basename
from pydantic import (BaseModel, BaseSettings)
from typing import Dict


class ModelConfig(BaseModel):
    pass

class Settings(BaseSettings):
    pass

settings = Settings()


datetime_format = "%Y-%m-%d %H:%M:%S"
time_format = "%Y%m%d_%H%M%S"

API_VERSION = "0.1.0"

# The API will erase temporary files (input txt and output ann) after each analyse request
TEMP_FILES_TTL_DAYS = 7


WEBAPP_ROOT_DIR = dirname(dirname(abspath(__file__)))
SITE_ROOT_DIR = dirname(WEBAPP_ROOT_DIR)
SITE_NAME = basename(WEBAPP_ROOT_DIR)

SETTINGS_DIR = dirname(abspath(__file__))

STORAGE_DIR = os.path.join(SITE_ROOT_DIR, "instance", "storage")
os.makedirs(STORAGE_DIR, exist_ok=True)

DATA_DIR = os.path.join(SITE_ROOT_DIR, "data")
os.makedirs(DATA_DIR, exist_ok=True)

WEBHOOK_EXTERNAL_IP = "127.0.0.1"
WEBHOOK_PORT = "80"

webhook_config = os.path.join(SETTINGS_DIR, "webhook.yaml")

########## LOGGING CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#logging
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
# Logging levels : CRITICAL | ERROR | WARNING | INFO | DEBUG | NOTSET
LOGLEVEL = os.environ.get('LOGLEVEL', 'info').upper()

LOGTRACE = False
if LOGLEVEL == 'TRACE':
    LOGLEVEL = 'DEBUG'
    LOGTRACE = True

LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            # exact format is not important, this is the minimum information
            'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        },
        'simple': {
            'format': '{levelname}:\t{message}',
            'style': '{',
        },
    },
    'filters': {
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
            'level': LOGLEVEL,
        },
    },
    'loggers': {
        'webapp': {
            'handlers': ['console'],
            'level': LOGLEVEL,
            'propagate': False,
        }
    },
    # 'root': {
    #    'level': LOGLEVEL,
    #    'handlers': ['console'],
    #}
}
########## END LOGGING CONFIGURATION
