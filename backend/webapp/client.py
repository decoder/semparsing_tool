#!/usr/bin/env python3
# coding: utf-8

import os
import sys

from typing import List

from server import embed_model
from server import concode_model


class ClientException(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return 'ClientException, {0} '.format(self.message)
        else:
            return 'ClientException has been raised'


class Client:
    def __init__(self):
        print(f"Client", file=sys.stderr)
        model_path = "/app/models/semparsing.pt"
        concode_java_path = "/app/models/model_acc_83.17_ppl_1.88_e10.pt"
        concode_cpp_path = "/app/models/model_acc_83.17_ppl_1.88_e10.pt"  # TODO set the correct value
        self.concode_java = concode_model.EmbedModels(model_path, concode_java_path)
        self.concode_cpp = concode_model.EmbedModels(model_path, concode_cpp_path)
        embed_model.init(model_path)

    def translate_java(self, text: str,
                       var_names: List[str] = [],
                       var_types: List[str] = [],
                       method_names: List[str] = [],
                       method_returns: List[str] = []) -> str:
        # configuration: ReformerConfiguration()):
        print(f"Client.translate_java {text}", file=sys.stderr)
        res = self.concode_java.translate([text], var_names, var_types,
                                          method_names, method_returns)
        return res

    def translate_cpp(self, text: str,
                       var_names: List[str] = [],
                       var_types: List[str] = [],
                       method_names: List[str] = [],
                       method_returns: List[str] = []) -> str:
        # configuration: ReformerConfiguration()):
        print(f"Client.translate_cpp {text}", file=sys.stderr)
        res = self.concode_cpp.translate([text],
                                         var_names, var_types,
                                         method_names, method_returns)
        return res

    def translate_ast(self, text: str) -> str:
        # configuration: ReformerConfiguration()):
        print(f"Client.translate_ast{text}", file=sys.stderr)
        res = embed_model.translate(text)
        return res


