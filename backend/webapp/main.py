#!/usr/bin/env python3
# coding: utf-8

import json
import sys

from logging import getLogger
from fastapi import (FastAPI)

from schemas.semparsing import Semparsing, SemparsingContext
from client import Client


app_logger = getLogger("webapp")

app = FastAPI()
app.state.client = Client()


@app.post("/ast/")
def ast(data: Semparsing):
    app_logger.info(f"/ast/ {data.text}")
    result = app.state.client.translate_ast(data.text)
    app_logger.debug(f"/ast result is `{result}`")
    return json.dumps(result)


@app.post("/java/")
def java(data: SemparsingContext):
    app_logger.info(f"/java/ {data.text}")
    result = app.state.client.translate_java(data.text,
                                             data.var_names, data.var_types,
                                             data.method_names, data.method_returns)
    app_logger.debug(f"/java/ result is `{result}`")
    return json.dumps(result)


@app.post("/cpp/")
def cpp(data: SemparsingContext):
    app_logger.info(f"/cpp/ {data.text}")
    result = app.state.client.translate_cpp(data.text)
    app_logger.debug(f"/cpp/ result is `{result}`")
    return json.dumps(result)


@app.post("/jml/")
def jml(data: Semparsing):
    print(f"/jml/ : {data}", file=sys.stderr)
    app_logger.info(f"/jml/ {data.text}")
    app_logger.info(f"jml {data.text}", file=sys.stderr)
    app_logger.info(f"There is currently no text to jml model. We give here some hard "
                    f"coded results for testing purpose.", file=sys.stderr)
    # text = encode(text)
    res = ""
    if data.text == "Retrieves the current account Balance":
        res = ("@ requires a != null ;\n"
               "@ ensures 0 <= \result ;\n"
               "@ ensures (\forall int i; 0 <= i < a.length; "
               "a[i] <= a[\result]);")
    elif data.text == ("Find the maximum element of an array by searching in from the "
                       "ends"):
        res = ("@ requires a != null && a.length > 0;\n"
               "@ ensures 0 <= \result < a.length;\n"
               "@ ensures (\forall int i; 0 <= i < a.length; "
               "a[i] <= a[\result]);")
    elif data.text == "Getter for member f":
        res = ("@pure")
    return json.dumps(res)
