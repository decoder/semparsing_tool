import datetime
import sys
from pydantic import (BaseModel, validator)
from typing import (Optional,
                    List,
                    Union,
                    Dict)


class SemparsingConfiguration(BaseModel):
    pass

class Semparsing(BaseModel):
    text: str

class SemparsingContext(Semparsing):
    text: str
    var_names: List[str]
    var_types: List[str]
    method_names: List[str]
    method_returns: List[str]

