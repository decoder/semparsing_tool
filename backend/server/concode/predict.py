import argparse
import os
import json
import subprocess

parser = argparse.ArgumentParser(description='Predict code knowing a '
                                 'description and a context')

# Prediction
parser.add_argument('-start', type=int, default=5,
                    help='Epoch to start prediction')
parser.add_argument('-end', type=int, default=30,
                    help='Epoch to end prediction')
parser.add_argument('-beam', type=int, default=3,
                    help='Beam size')
parser.add_argument('-tgt_len', type=int, default=3,
                    help='Beam size')

parser.add_argument('-models_dir', type=str, default='',
                    help='Models directory.')
parser.add_argument('-test_file', type=str, default='',
                    help='Test data directory.')
parser.add_argument('-additional', type=str, default='',
                    help='Any additional flags.')

opt = parser.parse_args()

os.makedirs(opt.models_dir + '/preds/', exist_ok=True)

# We need a text file with the outputs to compute BLEU.
# so extract it out of the json file
test_dataset = json.loads(open(opt.test_file, 'r').read())
test_dataset_targets = open('/tmp/test.code', 'w')
for example in test_dataset:
    test_dataset_targets.write(' '.join(example['code'])
                               .replace('concodeclass_', '')
                               .replace('concodefunc_', '') + '\n')
test_dataset_targets.close()

best_bleu, best_exact = (0, 0, 0), (0, 0, 0)
for i in range(opt.start, opt.end + 1):
    status, fname = subprocess.getstatusoutput(
      f"ls {opt.models_dir}/model_acc_*e{i}.pt")
    if status != 0:
        raise SystemExit(fname)
    # fname = !ls {opt.models_dir}/model_acc_*e{i}.pt
    fname = fname.split("\n")[0]
    print(fname)
    f = os.path.basename(fname)
    print(f)
    # !rm {opt.models_dir}/preds/{f}.nl.prediction*
    status, output = subprocess.getstatusoutput(
      f"rm -f {opt.models_dir}/preds/{f}.nl.prediction*")
    if status != 0:
        raise SystemExit(output)

    # Prod is just a dummy here
    translate = (f"python translate.py -beam_size {opt.beam} "
                 f"-model {fname} -src {opt.test_file} "
                 f"-output {opt.models_dir}/preds/{f}.nl.prediction "
                 f"-max_sent_length {opt.tgt_len} -replace_unk -batch_size 1 "
                 f"-trunc 2000 {opt.additional}")
    print(translate, flush=True)
    status, output = subprocess.getstatusoutput(translate)
    if status != 0:
        raise SystemExit(output)

    # bleu = !perl tools/multi-bleu.perl -lc /tmp/test.code
    # < {opt.models_dir}/preds/{f}.nl.prediction
    status, bleu = subprocess.getstatusoutput(
      f"perl tools/multi-bleu.perl -lc /tmp/test.code "
      f"< {opt.models_dir}/preds/{f}.nl.prediction")
    if status != 0:
        raise SystemExit(bleu)
    bleu = bleu.split("\n")
    print(bleu)
    bleu_score = float(bleu[0].split(',')[0])

    status, exact = subprocess.getstatusoutput(
      f"python tools/exact.py /tmp/test.code "
      f"< {opt.models_dir}/preds/{f}.nl.prediction")
    # exact = !python tools/exact.py /tmp/test.code
    # < {opt.models_dir}/preds/{f}.nl.prediction
    if status != 0:
        raise SystemExit(exact)
    print(exact)
    exact_score = float(exact.split("\n")[0])

    if bleu_score > best_bleu[0]:
        best_bleu = (bleu_score, exact_score, i)
    if exact_score > best_exact[1]:
        best_exact = (bleu_score, exact_score, i)
    print(f'Best BLEU so far is: {best_bleu[0]} - Exact is {best_bleu[1]} '
          f'- on epoch {best_bleu[2]}')
    print(f'BLEU is {best_exact[0]} - Best Exact so far: is {best_exact[1]} '
          f'- on epoch {best_exact[2]}')
