import time
import math
import sys


class Statistics(object):
    """
    Train/validate loss statistics.
    """
    def __init__(self, loss=0, n_words=0, n_correct=0, n_src_words=0):
        self.loss = loss
        self.n_words = n_words
        self.n_correct = n_correct
        self.n_src_words = n_src_words
        self.start_time = time.time()

    def update(self, stat):
        self.loss += stat.loss
        self.n_words += stat.n_words
        self.n_correct += stat.n_correct

    def __str__(self):
        return('Loss: {}, Words:{}, Correct:{} '.format(self.loss,
                                                        self.n_words,
                                                        self.n_correct))

    def accuracy(self):
        return 100 * (self.n_correct.item() / self.n_words.item())

    def ppl(self):
        return math.exp(min(self.loss / self.n_words, 100))

    def elapsed_time(self):
        return time.time() - self.start_time

    def output(self, epoch, batch, n_batches, start):
        t = self.elapsed_time()
        print(f"Epoch {epoch:>2}, {batch:>5}/{n_batches:>5}; "
              f"acc: {self.accuracy():6.2f}; ppl: {self.ppl():6.2f}; "
              f"{self.n_src_words / (t + 1e-5):3.0f} src tok/s; "
              f"{self.n_words / (t + 1e-5):3.0f} tgt tok/s; "
              f"{time.time() - start:6.0f} s elapsed",
              flush=True)
