#!/usr/bin/env python
# coding: utf-8

# In[1]:

import re


class CastPipeline(object):

    def __init__(self):

        self.parameters = ['referencetype', 'basictype']
        self.refliterals = [
            'catchclauseparameter', 'this', 'statement', 'typeparameter',
            'continuestatement', 'blockstatement', 'voidclassreference',
            'forcontrol', 'variabledeclarator',
            'explicitconstructorinvocation', 'superconstructorinvocation',
            'constructordeclaration', 'methodinvocation', 'returnstatement',
            'memberreference', 'supermethodinvocation', 'supermemberreference',
            'switchstatementcase', 'typeargument', 'basictype',
            'arrayinitializer', 'literal', 'breakstatement',
            'methoddeclaration', 'referencetype']


# ### V2

    def ast_to_cast_v2(self, ast):

        # process formal parmaters
        cast = self.formalparameter(ast)
        # process ref literals
        cast = self.Literal(cast)
        return cast

    def cast_to_ast_v2(self, cast):
        ast = self.inv_Literal(cast)
        ast = self.inv_formalparameter(ast)
        return ast

    def check_ast_cast_v2(self, ast):
        _ast = ast
        cast = self.ast_to_cast_v2(ast)
        ast = self.cast_to_ast_v2(cast)
        return len(ast.split()) == len(_ast.split()), _ast, cast, ast

    def formalparameter(self, ast):
        pmatch = re.compile(
          "\( formalparameter \( ([^\s]*) \) ([^\s]*) \) formalparameter")
        target = ast
        fm = []
        lst_indexes = [(m.start(), m.end(), m.group())
                       for m in pmatch.finditer(ast)]
        if len(lst_indexes) > 0:
            target = ast[:lst_indexes[0][0]]
            for i, ind in enumerate(lst_indexes):
                start, end, w = ind
                w0 = w.split()[3]

                if i == 0 and w0 in self.parameters:
                    w0 = f'{w0}_parameter'
                    target = target + w0
                elif w0 in self.parameters:
                    w0 = f'{w0}_parameter'
                    target = target + ast[_end:start] + w0
                _end = end
            target = target + ast[lst_indexes[-1][1]:]

        return target

    def inv_formalparameter(self, ast):
        target = [f"( formalparameter ( {s.split('_')[0]} ) "
                  f"{s.split('_')[0]} ) formalparameter"
                  if s.split('_')[0] in self.parameters and '_parameter' in s
                  else s for s in ast.split()]
        target = ' '.join(target)
        return target

    def Literal(self, ast):
        def process(t, ast):
            pmatch = re.compile("\( {} \) {}".format(t, t))
            target = ast
            lst_indexes = [(m.start(), m.end(), m.group())
                           for m in pmatch.finditer(ast)]
            # print(lst_indexes)
#             fm = list()
            if len(lst_indexes) > 0:

                target = ast[:lst_indexes[0][0]]
                for i, ind in enumerate(lst_indexes):
                    start, end, w = ind
                    if (w.split()[1] == w.split()[3]
                            and w.split()[1] in self.refliterals and i == 0):
                        w = f'{w.split()[1]}_ref'
                        target = target + w
                    elif (w.split()[1] == w.split()[3]
                          and w.split()[1] in self.refliterals):
                        w = f'{w.split()[1]}_ref'
                        target = target + ast[_end:start] + w
                    _end = end
                target = target + ast[lst_indexes[-1][1]:]
            return target

        for _type in self.refliterals:
            ast = process(_type, ast)

        return ast

    def inv_Literal(self, ast):
        target = [f"( {s.split('_')[0]} ) {s.split('_')[0]}"
                  if s.split('_')[0] in self.refliterals and '_ref' in s
                  else s for s in ast.split()]
        target = ' '.join(target)
        return target


# ### END V2
