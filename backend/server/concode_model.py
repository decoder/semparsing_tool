import os
import dill
import sys
import torch
from torchtext import data
from typing import List

from server.SimpleModel import Seq2seqAttn
from server.cast_pipeline import CastPipeline
from concode import translator


class EmbedModels():

    def __init__(self, model_path, concode_path):
        """
        Initialise the seq2seq model.
        This includes loading the model, which may take several minutes! This
        function must be called before any other function in this module.
        """

        self.cp = CastPipeline()
        self.device = torch.device('cpu')
        dirname = os.path.dirname(model_path)
        src_file = os.path.join(dirname, 'src.field')
        tgt_file = os.path.join(dirname, 'tgt.field')

        if (os.path.exists(model_path) and os.path.exists(src_file)
                and os.path.exists(tgt_file)):
            load_vars = torch.load(model_path, map_location=self.device)
            train_args = load_vars['train_args']
            model_params = load_vars['state_dict']

            self.SRC = self.load_field(src_file)
            self.TGT = self.load_field(tgt_file)
            fields = [('src', self.SRC), ('tgt', self.TGT)]

            self.model = Seq2seqAttn(train_args, True, fields,
                                     self.device).to(self.device)
            self.model.load_state_dict(model_params)

            self.model.eval()
        print(f"Instanciating Translator with path {concode_path}", flush=True)
        self.concode = translator.Translator(concode_path)

    def load_field(self, path):
        with open(path, 'rb') as f:
            return dill.load(f)

    def id2w(self, v, t):
        sentence = [v.vocab.itos[i] for i in t]
        if '<eos>' in sentence:
            sentence = sentence[:sentence.index('<eos>')]
        sentence = ' '.join(sentence)
        sentence = sentence.replace('<sos>', '')
        return sentence

    def encode(self, input):
        examples = [data.Example.fromlist([input], [('src', self.SRC)])]
        test_data = data.Dataset(examples, [('src', self.SRC)])
        test_iter = data.Iterator(test_data, batch_size=1,
                                  train=False, shuffle=False, sort=False,
                                  device=self.device)

        return test_iter

    def ast(self, text):
        text = self.encode(text)
        for samples in text:
            preds = self.model(samples.src,
                               tgts=None,
                               maxlen=100,
                               tf_ratio=0.0,
                               beam_search=True,
                               evaluation=True)
            preds = [self.id2w(self.TGT, pred) for pred in preds]
        cast = preds[0]
        _ast = self.cp.cast_to_ast_v2(cast.lower())
        return _ast

    def jml(self, text):
        print(f"embed_model.jml {text}", file=sys.stderr)
        print(f"There is currently no text to jml model. "
              f"We give here some hard "
              f"coded results for testing purpose.", file=sys.stderr)
        # text = encode(text)
        if text == "Retrieves the current account Balance":
            res = ("@ requires a != null ;\n"
                   "@ ensures 0 <= \result ;\n"
                   "@ ensures (\forall int i; 0 <= i < a.length; "
                   "a[i] <= a[\result]);")
        elif text == ("Find the maximum element of an array by searching in "
                      "from the ends"):
            res = ("@ requires a != null && a.length > 0;\n"
                   "@ ensures 0 <= \result < a.length;\n"
                   "@ ensures (\forall int i; 0 <= i < a.length; "
                   "a[i] <= a[\result]);")
        elif text == "Getter for member f":
            res = ("@pure")
        return res

    def acsl(self, text):
        # text = encode(text)
        return text

    def translate(self, nl: List[str],
                  var_names: List[str], var_types: List[str],
                  method_names: List[str], method_returns: List[str]) -> str:
        prediction = self.concode.translate(nl,
                                            var_names, var_types,
                                            method_names, method_returns)
        return '\n'.join([' '.join(pred) for pred in prediction])
