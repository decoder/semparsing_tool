# -*- coding: utf-8 -*-
"""
Created on Thu July 16 09:25:32 2020

@author: frejus
"""

import json

"""
Network communication message format.
This implementation: JSON RPC 2.0 with named parameters in the request objects.
"""

def decode_request(body):
    """
    Decode a request message to a dictionary containig the request information.
    Arguments:
        body:  the body of a message as received over the wire
    Returns:
        A dictionary with the three keys 'method' and 'params'. The following two
        combination of values are possible:
            - {method='ast', params={text=<str>}}
            - {method='jml', params={text=<str>}}
            - {method='acsl', params={text=<str>}}
    """
    j = json.loads(body)
    print('j: {}'.format(j['method']))

    if j['method'] == 'encode':
        if not 'text' in j['params']:
            raise Exception(_missing_param('text', body))
        method = 'encode'
        params = dict(text=j['params']['text'])

    elif j['method'] == 'ast':
        if not 'text' in j['params']:
            raise Exception(_missing_param('text', body))
        method = 'ast'
        p = j['params']
        params = dict(text=p['text'])

    elif j['method'] == 'jml':
        if not 'text' in j['params']:
            raise Exception(_missing_param('text', body))
        method = 'jml'
        p = j['params']
        params = dict(text=p['text'])
    elif j['method'] == 'java':
        if 'nl' not in j['params']:
            raise Exception(_missing_param('nl', body))
        if 'var_names' not in j['params']:
            raise Exception(_missing_param('var_names', body))
        if 'var_types' not in j['params']:
            raise Exception(_missing_param('var_types', body))
        if'method_names' not in j['params']:
            raise Exception(_missing_param('method_names', body))
        if 'method_returns' not in j['params']:
            raise Exception(_missing_param('method_returns', body))
        method = 'java'
        p = j['params']
        params = dict(nl=p['nl'],
                      var_names=p['var_names'],
                      var_types=p['var_types'],
                      method_names=p['method_names'],
                      method_returns=p['method_returns'])
    elif j['method'] == 'acsl':
        if 'text' not in j['params']:
            raise Exception(_missing_param('text', body))
        method = 'acsl'
        p = j['params']
        params = dict(text=p['text'])

    else:
        raise Exception("Invalid request method: " + j['method'])

    return dict(method=method, params=params)

def encode_response(obj, context):
    """
    Encode a response object to a message body to be sent over the wire.
    Arguments:
        obj:      any object to be sent as the result to the server
        context:  the request (before decoding) to which this message responds
    Returns:
        An encoded message body.
    """
    return json.dumps(dict(result=obj, id=json.loads(context)['method']))
    #return json.dumps(dict(result=obj))


def _missing_param(p, body):
    return "Param '%s' missing in: %s" % (p, body)
