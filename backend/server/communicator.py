# -*- coding: utf-8 -*-
"""
Created on Thu July 16 09:25:32 2020

@author: Frejus Laleye
"""


import encoder
import socket
import sys
import threading


"""
Network communication with the client.
This implemenation uses socket as a message broker.
"""


_channel = None
_on_encode = None
_on_ast = None
_on_jml = None
_on_acsl = None
_on_java = None
_on_cpp = None


class ClientThread(threading.Thread):

    def __init__(self, ip, port, clientsocket):

        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.clientsocket = clientsocket
        print(f"[+] New thread for {self.ip}:{self.port}", file=sys.stderr)

    def run(self):

        print(f"ClientThread.run Connection of {self.ip}:{self.port}",
              file=sys.stderr)
        msg = ''
        while True:
            print(f"ClientThread.run waiting for chunk", file=sys.stderr)
            chunk = self.clientsocket.recv(2048)
            chunk = chunk.decode("utf-8")
            if not chunk:
                break
            if chunk.endswith("SPEOF"):
                print(f"ClientThread.run got last chunk '{chunk}'",
                      file=sys.stderr)
                msg = msg + chunk[:len(chunk)-len("SPEOF")]
                break
            print(f"ClientThread.run got chunk '{chunk}'", file=sys.stderr)
            msg = msg + chunk

        resp = self._on_request(msg)
        self.clientsocket.send(str.encode(resp))

        print("Client disconnected...", file=sys.stderr)
        self.clientsocket.close()

    def _on_request(self, body):
        """
        Callback function called whenever a message is consumed from the queue.
        """
        # Decode and handle request
        print(f"ClientThread._on_request [x] Receiving: {body}",
              file=sys.stderr)
        decoded = encoder.decode_request(body)
        if decoded['method'] == 'encode':
            result = _on_encode(decoded['params']['text'])
        elif decoded['method'] == 'ast':
            p = decoded['params']
            result = _on_ast(p['text'])
        elif decoded['method'] == 'jml':
            p = decoded['params']
            result = _on_jml(p['text'])
        elif decoded['method'] == 'acsl':
            p = decoded['params']
            result = _on_acsl(p['text'])
        elif decoded['method'] == 'java':
            p = decoded['params']
            result = _on_java.java(p['nl'],
                                   p['var_names'], p['var_types'],
                                   p['method_names'], p['method_returns'])
        elif decoded['method'] == 'cpp':
            p = decoded['params']
            result = _on_cpp.cpp(p['nl'],
                                   p['var_names'], p['var_types'],
                                   p['method_names'], p['method_returns'])
        # Encode and return response

        print(f"ClientThread._on_request [ ] Result: {result}",
              file=sys.stderr)
        response = encoder.encode_response(result, body)
        print(f"ClientThread._on_request [ ] Returning: {response}",
              file=sys.stderr)
        return response


def register_on_encode(fun):
    """
    Register function to be called when an "encode" request is received.
    Arguments:
        fun:  a function taking the following arguments:
                text:  the text to be encoded
    """
    global _on_encode
    _on_encode = fun


def register_on_ast(fun):
    """
    Register function to be called when a "ast" request is received.
    Arguments:
        fun:  a function taking the following arguments:
                text:  the query sentence
    """
    global _on_ast
    _on_ast = fun


def register_on_jml(fun):
    """
    Register function to be called when a "jml" request is received.
    Arguments:
        fun:  a function taking the following arguments:
                text:  the query sentence

    """
    global _on_jml
    _on_jml = fun


def register_on_java(obj):
    """
    """
    global _on_java
    _on_java = obj


def register_on_acsl(fun):
    """
    Register function to be called when a "acsl" request is received.
    Arguments:
        fun:  a function taking the following arguments:
                text:  the query sentence
    """
    global _on_acsl
    _on_acsl = fun


def register_on_cpp(fun):
    """
    Register function to be called when a "cpp" request is received.
    Arguments:
        fun:  a function taking the following arguments:
                text:  the query sentence
    """
    global _on_cpp
    _on_cpp = fun


def start_listening():
    """
    Start listening for requests from the client.
    This function must be called after the "init" and "register_..." functions.
    This call is blocking!
    """
    print(f"ClientThread.start_listening [X] Awaiting RPC requests")
#    _channel.start_consuming()

    tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    tcpsock.bind((socket.gethostbyname('semanticparsing_backend'), 1028))

    while True:
        tcpsock.listen(10)
        print("ClientThread.start_listening Listening...")
        (clientsocket, (ip, port)) = tcpsock.accept()
        newthread = ClientThread(ip, port, clientsocket)
        newthread.start()
