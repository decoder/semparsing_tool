import os
import dill
import sys
import torch
from torchtext import data
# from model import Seq2seqAttn
from server.SimpleModel import Seq2seqAttn
from server.cast_pipeline import CastPipeline


_encoder = None

MODEL_PATH_EXISTS = False


cp = CastPipeline()


def load_field(path):
    with open(path, 'rb') as f:
        return dill.load(f)


def id2w(v, t):
    sentence = [v.vocab.itos[i] for i in t]
    if '<eos>' in sentence:
        sentence = sentence[:sentence.index('<eos>')]
    sentence = ' '.join(sentence)
    sentence = sentence.replace('<sos>', '')
    return sentence


def init(model_path):
    """
    Initialise the seq2seq model.
    This includes loading the model, which may take several minutes! This
    function must be called before any other function in this module.
    """
    global _encoder, model, SRC, TGT, device

    device = torch.device('cpu')
    dirname = os.path.dirname(model_path)
    src_file = os.path.join(dirname, 'src.field')
    tgt_file = os.path.join(dirname, 'tgt.field')

    if (os.path.exists(model_path) and os.path.exists(src_file)
            and os.path.exists(tgt_file)):
        load_vars = torch.load(model_path, map_location=device)
        train_args = load_vars['train_args']
        model_params = load_vars['state_dict']

        SRC = load_field(src_file)
        TGT = load_field(tgt_file)
        fields = [('src', SRC), ('tgt', TGT)]

        model = Seq2seqAttn(train_args, True, fields, device).to(device)
        model.load_state_dict(model_params)

        model.eval()
        MODEL_PATH_EXISTS = True
    else:
        MODEL_PATH_EXISTS = False


def encode(input):
    examples = [data.Example.fromlist([input], [('src', SRC)])]
    test_data = data.Dataset(examples, [('src', SRC)])
    test_iter = data.Iterator(test_data, batch_size=1,
                              train=False, shuffle=False, sort=False,
                              device=device)

    return test_iter


def ast(text):
    text = encode(text)
    for samples in text:
        preds = predict(samples)
    cast = preds[0]
    _ast = cp.cast_to_ast_v2(cast.lower())
    return _ast


def jml(text: str) -> str:
    print(f"embed_model.jml {text}", file=sys.stderr)
    print(f"There is currently no text to jml model. We give here some hard "
          f"coded results for testing purpose.", file=sys.stderr)
    # text = encode(text)
    res = ""
    if text == "Retrieves the current account Balance":
        res = ("@ requires a != null ;\n"
               "@ ensures 0 <= \result ;\n"
               "@ ensures (\forall int i; 0 <= i < a.length; "
               "a[i] <= a[\result]);")
    elif text == ("Find the maximum element of an array by searching in from "
                  "the ends"):
        res = ("@ requires a != null && a.length > 0;\n"
               "@ ensures 0 <= \result < a.length;\n"
               "@ ensures (\forall int i; 0 <= i < a.length; "
               "a[i] <= a[\result]);")
    elif text == "Getter for member f":
        res = ("@pure")
    return res


def acsl(text: str) -> str:
    # text = encode(text)

    return ""


def predict(samples):
    preds = model(samples.src, tgts=None, maxlen=100, tf_ratio=0.0,
                  beam_search=True, evaluation=True)

    # preds = preds.max(2)[1].transpose(1, 0)
    outs = [id2w(TGT, pred) for pred in preds]

    return outs
