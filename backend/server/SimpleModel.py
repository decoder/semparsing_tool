# -*- coding: utf-8 -*-

import random, sys
import operator

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

#from components import get_astEmbeddings

from queue import PriorityQueue

CUDA = torch.cuda.is_available()
Tensor = torch.cuda.FloatTensor if CUDA else torch.FloatTensor
LongTensor = torch.cuda.LongTensor if CUDA else torch.LongTensor


class EncRNN(nn.Module):
    def __init__(self, vsz, pretrained, TEXT, embed_dim, hidden_dim,
                 n_layers, use_birnn, dout, network, device,
                 update_embedding=False):
        super(EncRNN, self).__init__()

        if not pretrained:
            self.embed = nn.Embedding(vsz, embed_dim)
        else:
            self.embed = nn.Embedding(vsz, embed_dim).from_pretrained(TEXT.vocab.vectors)
        self.embed.weight.requires_grad = update_embedding

        if network == 'lstm':
            self.rnn = nn.LSTM(embed_dim, hidden_dim, n_layers, dropout=dout,
                           bidirectional=use_birnn)
        else:
            self.rnn = nn.GRU(embed_dim, hidden_dim, n_layers, dropout=dout,
                           bidirectional=use_birnn)
        self.dropout = nn.Dropout(dout)
        self.fc = nn.Linear(hidden_dim * 2, hidden_dim)
        self.num_layers = n_layers
        self.bidirectional = use_birnn
        self.network = network
        self.device = device
        self.hidden_dim = hidden_dim


    def forward(self, inputs, encoder_hidden, lengths=None):

        embs = self.dropout(self.embed(inputs)) # comment for cancel current translation
        if lengths is not None:
            packed = pack_padded_sequence(embs, lengths, batch_first=True)
            enc_outs, hidden = self.rnn(packed, encoder_hidden)
            enc_outs, _ = pad_packed_sequence(enc_outs, batch_first=True)
        else:
            enc_outs, hidden = self.rnn(embs, encoder_hidden) # comment for cancel current translation

        return enc_outs, hidden

    def initHidden(self, batch_size=1):
        rnn_stack_size = self.num_layers
        if self.bidirectional:
            rnn_stack_size *= 2
        if self.network == 'gru':
            return torch.zeros(rnn_stack_size, batch_size, self.hidden_dim, device=self.device)
        else:
            return (torch.zeros(rnn_stack_size, batch_size, self.hidden_dim, device=self.device),
                    torch.zeros(rnn_stack_size, batch_size, self.hidden_dim, device=self.device))


class Attention(nn.Module):
    def __init__(self, hidden_dim, embed_dim, method):
        super(Attention, self).__init__()
        self.method = method
        self.hidden_dim = hidden_dim
        self.embed_dim = embed_dim
        self.max_len = 50

        if method == 'general':
            self.w = nn.Linear(hidden_dim, hidden_dim)
        elif method == 'concat':
            self.attn = nn.Linear(hidden_dim*2 + hidden_dim, hidden_dim)

            self.w = nn.Linear(hidden_dim, 1, bias=False)

    def forward(self, hidden, enc_outs):

        if self.method == 'dot':
            attn_energies = self.dot(dec_out, enc_outs)
        elif self.method == 'general':
            attn_energies = self.general(dec_out, enc_outs)
        elif self.method == 'concat':
            attn_energies = self.concat(enc_outs, hidden)

        return F.softmax(attn_energies, dim=1)

    def dot(self, dec_out, enc_outs):
        return torch.sum(dec_out*enc_outs, dim=2)

    def general(self, dec_out, enc_outs):
        energy = self.w(enc_outs)
        return torch.sum(dec_out*energy, dim=2)

    def concat(self, enc_outs, hidden):
        src_len = enc_outs.shape[0]
        batch_size = enc_outs.shape[1]
        #print('out0', enc_outs.shape, hidden.shape)
        hidden = hidden.unsqueeze(1).repeat(1, src_len, 1)
        enc_outs = enc_outs.permute(1, 0, 2)
        energy = torch.cat((enc_outs, hidden), 2)
        energy = self.attn(energy).tanh()
        attn_tensor = self.w(energy).squeeze(2)
        return attn_tensor


class DecRNN(nn.Module):
    def __init__(self, vsz, pretrained, embed_dim, hidden_dim, n_layers, use_birnn,
                 dout, attn, tied, network, device, external_lm_dim):
        super(DecRNN, self).__init__()

        if pretrained is None:
            self.embed = nn.Embedding(vsz, embed_dim)
        else:
            self.embed = pretrained

        dec_hidden_dim = hidden_dim

        if network == 'lstm':
            self.rnn = nn.LSTM(embed_dim, hidden_dim , n_layers, dropout=dout)
        elif network == 'gru':
            self.rnn = nn.GRU(hidden_dim*2 + embed_dim, dec_hidden_dim, dropout=dout)

        self.w = nn.Linear(hidden_dim*2, hidden_dim)
        self.attn = Attention(hidden_dim, embed_dim, attn)
        self.attn_combine = nn.Linear(embed_dim+hidden_dim, embed_dim)
        self.device = device
        self.hidden_dim = hidden_dim
        self.network = network
        self.embed_dim = embed_dim

        self.out_projection = nn.Linear(hidden_dim*2+ dec_hidden_dim + embed_dim, vsz)
        if tied:
            if embed_dim != hidden_dim:
                raise ValueError(
                    f"when using the tied flag, embed-dim:{embed_dim} \
                    must be equal to hidden-dim:{hidden_dim}")
            self.out_projection.weight = self.embed.weight
        self.dropout = nn.Dropout(dout)

    def forward(self, inputs, hidden, encoder_outputs, joint_decoding=False, lmout=None, beam_decoding=False):
        inputs = inputs.unsqueeze(0)
        embs = self.dropout(self.embed(inputs))
        if beam_decoding:
            embs = embs.squeeze(0)

        if self.network == 'lstm':
            attn_weights = self.attn(hidden[0], encoder_outputs) # comment for cancel current translation
        elif self.network == 'gru':
            attn_weights = self.attn(hidden, encoder_outputs) # comment for cancel current translation
        attn_weights = attn_weights.unsqueeze(1)

        encoder_outputs = encoder_outputs.permute(1, 0, 2)

        context = torch.bmm(attn_weights, encoder_outputs).permute(1, 0, 2)

        cats = torch.cat((embs, context), dim=2) # comment for cancel current translation
        output, hidden = self.rnn(cats, hidden.unsqueeze(0))  # comment for cancel current translation
        #print('fin', output.shape, hidden.shape)
        assert (output == hidden).all()

        embs = embs.squeeze(0)
        output = output.squeeze(0)
        context = context.squeeze(0)
        output = self.out_projection(torch.cat((output, context, embs), dim=1))



        return output, hidden.squeeze(0)

    def initState(self, batch_size=1):
        return torch.zeros( 1, batch_size, self.hidden_dim, device=self.device)

def id2w(pred, field):
    sentence = [field.vocab.itos[i] for i in pred]
    if '<eos>' in sentence:
        return ' '.join(sentence[:sentence.index('<eos>')])
    return ' '.join(sentence)


def maskset(x, PAD_IDX):
    print(PAD_IDX)
    if type(x) == torch.Tensor:
        mask = x.eq(PAD_IDX)
        lens = x.size(1) - mask.sum(1)
        return mask, lens
    mask = Tensor([[1] * i + [PAD_IDX] * (x[0] - i) for i in x]).eq(PAD_IDX)
    return mask, x


class Seq2seqAttn(nn.Module):
    def __init__(self, args, encoder_embeddings, fields, device,
                 decoder_pretrained_embeddings=False):
        super().__init__()
        self.src_field, self.tgt_field = fields
        self.pad_idx = self.tgt_field[1].vocab.stoi[self.tgt_field[1].pad_token]
        self.sos_idx = self.tgt_field[1].vocab.stoi[self.tgt_field[1].init_token]
        self.src_vsz = len(self.src_field[1].vocab.itos)
        self.tgt_vsz = len(self.tgt_field[1].vocab.itos)
        self.lm = None

        if decoder_pretrained_embeddings:
            decoder_embeddings, ast_lm = get_astEmbeddings(
                args.decoder_embeddings_filename,
                args.decoder_embeddings_vocab_filename,
                args.decoder_embed_dim,
                args.decoder_embed_nhead,
                args.decoder_embed_nhid,
                args.decoder_embed_nlayers,
                device,
                args.decoder_embed_dropout)
            decoder_embed_dim = args.decoder_embed_dim
            self.lm = ast_lm
        else:
            decoder_embeddings = None
            decoder_embed_dim = args.embed_dim

        enc_bidirectional = args.bidirectional
        dec_bidirectional = False
        self.network = 'gru'
        self.encoder = EncRNN(self.src_vsz, encoder_embeddings,
                              self.src_field[1], args.embed_dim,
                              args.hidden_dim, args.n_layers,
                              enc_bidirectional, args.dropout,
                              self.network, device, args.update_embedding)

        if self.lm is not None:
            external_lm_dim = self.lm.vocab_size
        else:
            external_lm_dim = None
        self.decoder = DecRNN(self.tgt_vsz, decoder_embeddings,
                              decoder_embed_dim, args.hidden_dim,
                              args.n_layers, enc_bidirectional, args.dropout,
                              args.attn, args.tied, self.network, device,
                              external_lm_dim)

        self.device = device
        self.n_layers = args.n_layers
        self.hidden_dim = args.hidden_dim
        self.use_birnn = args.bidirectional
        self.fc = nn.Linear(self.hidden_dim * 2, self.hidden_dim)
        self.padding = args.padding

    def forward(self, srcs, tgts=None, tf_ratio=0.0, maxlen=100, tgt=None,
                beam_search=False, evaluation=False, joint_decoding=False):
        slen, bsz = srcs.size()

        tlen = tgts.size(0) if isinstance(tgts, torch.Tensor) else maxlen
        tf_ratio = tf_ratio if isinstance(tgts, torch.Tensor) else 0.0

        #print(slen, tlen, bsz)
        encoder_hidden = self.encoder.initHidden(bsz)
        if self.padding:
            mask, lens = maskset(srcs, self.pad_idx)
        else:
            lens = None
        enc_outs, encoder_hidden  = self.encoder(srcs, encoder_hidden,
                                                 lengths=lens)
        lmout = None
        if self.network == 'lstm':
            encoder_hidden = (encoder_hidden, self.decoder.initState(bsz))

        decoder_hidden = self._init_state(encoder_hidden)

        if beam_search:
            outs = self.beam_decode(srcs, decoder_hidden, enc_outs)
            return outs
        else:
            if tgts is None:
                dec_inputs = torch.ones_like(srcs[0]) * self.sos_idx # <eos> is mapped to id=2
            else:
                dec_inputs = tgts[0,:]

            outs = torch.zeros(tlen, bsz, self.tgt_vsz).to(self.device)

            for i in range(1, tlen):


                if joint_decoding and self.lm is not None:
                    lmout, lmout_tokens, lmout_idx = self.lm.predict(
                      dec_inputs, is_tensor=True)

                preds, decoder_hidden = self.decoder(dec_inputs,
                                                     decoder_hidden, enc_outs,
                                                     joint_decoding, lmout)

                outs[i] = preds
                use_tf = random.random() < tf_ratio
                dec_inputs = tgts[i] if use_tf else preds.argmax(1)

                if evaluation and dec_inputs[0].item() == 3: # eos idx is 3
                    return outs[:i]
            return outs


    def _init_state(self, encoder_hidden):
        """ Initialize the encoder hidden state. """
        if encoder_hidden is None:
            return None
        if isinstance(encoder_hidden, tuple):
            encoder_hidden = tuple([
              self._cat_directions(h) for h in encoder_hidden])
        else:
            encoder_hidden = self._cat_directions(encoder_hidden)
        return encoder_hidden

    def _cat_directions(self, h):
        """ If the encoder is bidirectional, do the following transformation.
            (#directions * #layers, #batch, hidden_size) -> (#layers, #batch, #directions * hidden_size)
        """
        if self.encoder.bidirectional:

            cats = torch.cat((h[-2,:,:], h[-1,:,:]), dim = 1)
            fc = self.fc(cats)
            h = torch.tanh(fc)
        return h



    def beam_decode(self, target_tensor, decoder_hiddens, encoder_outputs=None):
        '''
        :param target_tensor: target indexes tensor of shape [B, T] where B is the batch size and T is the maximum length of the output sentence
        :param decoder_hidden: input tensor of shape [1, B, H] for start of the decoding
        :param encoder_outputs: if you are using attention mechanism you can pass encoder outputs, [T, B, H] where T is the maximum length of input sentence
        :return: decoded_batch
        '''

        beam_width = 3
        topk = 1  # how many sentence do you want to generate
        decoded_batch = []
        for idx in range(target_tensor.size(1)):
            if isinstance(decoder_hiddens, tuple):  # LSTM case
                decoder_hidden = (decoder_hiddens[0][:,idx, :].unsqueeze(0),
                                  decoder_hiddens[1][:,idx, :].unsqueeze(0))
            else:
                #decoder_hidden = decoder_hiddens[:, idx, :].unsqueeze(0)
                decoder_hidden = decoder_hiddens[idx, :].unsqueeze(0)

            encoder_output = encoder_outputs[:,idx, :].unsqueeze(1)

            # Start with the start of the sentence token
            decoder_input = torch.LongTensor([[2]], device=self.device) #sos token is 2

            # Number of sentence to generate
            endnodes = []
            number_required = min((topk + 1), topk - len(endnodes))

            # starting node -  hidden vector, previous node, word id, logp, length
            node = BeamSearchNode(decoder_hidden, None, decoder_input, 0, 1)
            nodes = PriorityQueue()

            # start the queue
            nodes.put((-node.eval(), node))
            qsize = 1

            # start beam search
            while True:
                # give up when decoding takes too long
                if qsize > 2000: break

                # fetch the best node
                score, n = nodes.get()
                decoder_input = n.wordid
                decoder_hidden = n.h

                if n.wordid.item() == 3 and n.prevNode != None: # eos token is 3
                    endnodes.append((score, n))
                    # if we reached maximum # of sentences required
                    if len(endnodes) >= number_required:
                        break
                    else:
                        continue

                # decode for one step using decoder
                decoder_output, decoder_hidden = self.decoder(
                  decoder_input, decoder_hidden, encoder_output,
                  beam_decoding=True)
                # PUT HERE REAL BEAM SEARCH OF TOP
                log_prob, indexes = torch.topk(decoder_output, beam_width)
                nextnodes = []

                for new_k in range(beam_width):
                    decoded_t = indexes[0][new_k].view(1, -1)
                    log_p = log_prob[0][new_k].item()

                    node = BeamSearchNode(decoder_hidden, n, decoded_t,
                                          n.logp + log_p, n.leng + 1)
                    score = -node.eval()
                    nextnodes.append((score, node))
                try:
                    # put them into queue
                    for i in range(len(nextnodes)):
                        score, nn = nextnodes[i]
                        nodes.put((score, nn))
                except:
                    print('queue error')
                    continue
                    # increase qsize
                qsize += len(nextnodes) - 1

            # choose nbest paths, back trace them
            if len(endnodes) == 0:
                endnodes = [nodes.get() for _ in range(topk)]

            utterances = []
            for score, n in sorted(endnodes, key=operator.itemgetter(0)):
                utterance = []
                utterance.append(n.wordid[0][0]) # use wordid[][] instead of wordid
                # back trace
                while n.prevNode != None:
                    n = n.prevNode
                    utterance.append(n.wordid[0][0])
                utterance = utterance[::-1]
                utterances.append(utterance)
            decoded_batch.append(utterances[0])

        return decoded_batch


class BeamSearchNode(object):
    def __init__(self, hiddenstate, previousNode, wordId, logProb, length):
        '''
        :param hiddenstate:
        :param previousNode:
        :param wordId:
        :param logProb:
        :param length:
        '''
        self.h = hiddenstate
        self.prevNode = previousNode
        self.wordid = wordId
        self.logp = logProb
        self.leng = length

    def eval(self, alpha=1.0):
        reward = 0
        # Add here a function for shaping a reward

        return self.logp / float(self.leng - 1 + 1e-6) + alpha * reward

