#!/usr/bin/env python
# coding: utf-8

# In[1]:


import re

class CastPipeline(object):
    
    def __init__(self):
        
        self.ltypes = ['stringliteral', 'booleanliteral', 'numberliteral', 'nullliteral', 'characterliteral', 'integerliteral', 'floatingpointliteral']
        self.modifiers = ['public', 'protected', 'annotation', 'private', 'abstract', 'static', 'final', 'native', 'synchronized',
                          'transient', 'volatile', 'strictfp']
        
    def ast_to_cast(self, ast):
        #process simpletype
        cast = self.simpletype(ast)
        print('simpletye', len(cast.split()))
        #process methodinvocation
        cast = self.methodinvocation(cast)
        print('methodinvocation',len(cast.split()))
        #primitivetype
        cast = self.primitivetype(cast)
        print('primitivetype',len(cast.split()))
        #modifierpublic
        cast = self.modifierpublic(cast)
        print('modifierpublic',len(cast.split()))
        #blockstatement
        cast = self.blockstatement(cast)
        print('blockstatement', len(cast.split()))
        #XLiteral
        cast = self.XLiteral(cast)
        print('XLiteral', len(cast.split()))
        #simple_name
        cast = self.simplename(cast)
        print('simple_name', len(cast.split()))
        
        return cast
    
    def cast_to_ast(self, cast):
        
        #inv processsimple_name
        ast = self.inv_simplename(cast)
        print('simple_name', len(ast.split()))
        #inv processXLiteral
        ast = self.inv_XLiteral(ast)
        print('processXLiteral', len(ast.split()))
        #inv process blockstatement
        ast = self.inv_blockstatement(ast)
        print('blockstatement', len(ast.split()))
        #inv process modifierpublic
        ast = self.inv_modifierpublic(ast)
        print('modifierpublic', len(ast.split()))
        #inv process primitivetype
        ast = self.inv_primitivetype(ast)
        print('primitivetype', len(ast.split()))
        #inv process methodinvocation
        ast = self.inv_methodinvocation(ast)
        print('methodinvocation', len(ast.split()))
        #inv process simpletype
        ast = self.inv_simpletype(ast)
        print('simpletype', len(ast.split()))
        
        return ast
    
    def debug_ast_cast(self, ast):
        print('simpletype:')
        _ast = ast
        cast = self.simpletype(ast)
        ast = self.inv_simpletype(cast)
        print(f'simpletype: len(ast):{len(ast.split())} len(_ast){len(_ast.split())}')
        assert len(ast.split()) == len(_ast.split())
        _ast = cast
        print('methodinvocation:')
        cast = self.methodinvocation(_ast)
        ast = self.inv_methodinvocation(cast)
        print(f'methodinvocation: len(ast):{len(ast.split())} len(_ast){len(_ast.split())}')
        assert len(ast.split()) == len(_ast.split())
        _ast = cast
        print('primitivetype:')
        cast = self.primitivetype(_ast)
        ast = self.inv_primitivetype(cast)
        print(f'primitivetype: len(ast):{len(ast.split())} len(_ast){len(_ast.split())}')
        assert len(ast.split()) == len(_ast.split())
        
        _ast = cast
        print('modifierpublic:')
        cast = self.modifierpublic(_ast)
        ast = self.inv_modifierpublic(cast)
        print(f'modifierpublic: len(ast):{len(ast.split())} len(_ast){len(_ast.split())}')
        assert len(ast.split()) == len(_ast.split())
        
        _ast = cast
        print('blockstatement:')
        cast = self.blockstatement(_ast)
        ast = self.inv_blockstatement(cast)
        print(f'blockstatement: len(ast):{len(ast.split())} len(_ast){len(_ast.split())}')
        assert len(ast.split()) == len(_ast.split())
        _ast = cast
        print('XLiteral:')
        cast = self.XLiteral(_ast)
        ast = self.inv_XLiteral(cast)
        print(f'XLiteral: len(ast):{len(ast.split())} len(_ast){len(_ast.split())}')
        assert len(ast.split()) == len(_ast.split())
        _ast = cast
        print('simplename:')
        cast = self.simplename(_ast)
        ast = self.inv_simplename(cast)
        print(f'simplename: len(ast):{len(ast.split())} len(_ast){len(_ast.split())}')
        assert len(ast.split()) == len(_ast.split())
        
    
    
    def check_ast_cast(self, ast):
        _ast = ast
        print(len(ast))
        cast = self.ast_to_cast(ast)
        print(len(cast))
        ast = self.cast_to_ast(cast)
        print(len(ast))
        return len(ast) == len(_ast)
    
    def simpletype(self, ast):
        pmatch = re.compile("\( simpletype \( simplename_([^\s]*) \) simplename_([^\s]*) \) simpletype")
        target = ast
        
        lst_indexes = [(m.start(), m.end(), m.group() )for m in pmatch.finditer(ast)]
        if len(lst_indexes) > 0:
            target = ast[:lst_indexes[0][0]]
    #         print(target)
            for i, ind in enumerate(lst_indexes):
                start, end, w = ind
                w0 = w.split()[1]
                w = w.split()[3].split('_')
                w = '_'.join(w[1:])
                w = f'{w0}_{w}'
                if i == 0:
                    target = target + w
    #                 print(target)
                else:
                    target = target + ast[_end:start] + w
                _end = end
            target = target + ast[lst_indexes[-1][1]:]

        return target
    
    def inv_simpletype(self, ast):
        pmatch = re.compile(" simpletype_([^\s]*)")
        target = ast
        
        lst_indexes = [(m.start(), m.end(), m.group() )for m in pmatch.finditer(ast)]
        if len(lst_indexes) > 0:
            target = ast[:lst_indexes[0][0]]
            for i, ind in enumerate(lst_indexes):
                start, end, w = ind
                w = w.split()[0].split('_')
                w = '_'.join(w[1:])
                w = f" ( simpletype ( simplename_{w} ) simplename_{w} ) simpletype"
                if i == 0:
                    target = target + w
                else:
                    target = target + ast[_end:start] + w
                _end = end
            target = target + ast[lst_indexes[-1][1]:]
        return target
    
    def methodinvocation(self, ast):
        pmatch = re.compile("\( methodinvocation \( simplename_([^\s]*) \) simplename_([^\s]*)")
        target = ast
        lst_indexes = [(m.start(), m.end(), m.group() )for m in pmatch.finditer(ast)]
        
        if len(lst_indexes) > 0:
            target = ast[:lst_indexes[0][0]]
            for i, ind in enumerate(lst_indexes):
                start, end, w = ind
                w0 = w.split()[1]
                w = w.split()[3].split('_')
                w = '_'.join(w[1:])
                w = f'( {w0}_{w})'
                if i == 0:
                    target = target + w
                else:
                    target = target + ast[_end:start] + w
                _end = end
            target = target + ast[lst_indexes[-1][1]:]

        return target

    def inv_methodinvocation(self, ast):
        pmatch = re.compile("\( methodinvocation_([^\s]*)")
        target = ast
        
        lst_indexes = [(m.start(), m.end(), m.group() )for m in pmatch.finditer(ast)]
        
        if len(lst_indexes) > 0:
            
            target = ast[:lst_indexes[0][0]]
            for i, ind in enumerate(lst_indexes):
                start, end, w = ind
                w = f"( methodinvocation ( simplename_{'_'.join(w.split('_')[1:])} )  simplename_{'_'.join(w.split('_')[1:])}"
                if i == 0:
                    target = target + w
                else:
                    target = target + ast[_end:start] + w
                _end = end
            target = target + ast[lst_indexes[-1][1]:]

        return target
    
    def primitivetype(self, ast):
        pmatch = re.compile("\( primitivetype \) primitivetype")
        target = ast
        
        lst_indexes = [(m.start(), m.end(), m.group() )for m in pmatch.finditer(ast)]
        
        if len(lst_indexes) > 0:
            
            target = ast[:lst_indexes[0][0]]
            for i, ind in enumerate(lst_indexes):
                start, end, w = ind
                w = f"{w.split()[1]}"
                if i == 0:
                    target = target + w
                else:
                    target = target + ast[_end:start] + w
                _end = end
            target = target + ast[lst_indexes[-1][1]:]

        return target
    
    

    def inv_primitivetype(self, ast):
        pmatch = re.compile("primitivetype")
        target = ""
        for m in ast.split():
            if m == "primitivetype":
                m = f"( primitivetype ) primitivetype"
            target += "{} ".format(m)
        return target
    
    
    def modifierpublic(self, ast):
        
        def process(val, ast):
            m = "\( modifier_{} \) modifier_{}".format(val, val)
            pmatch = re.compile(m)
            target = ast
            lst_indexes = [(m.start(), m.end(), m.group() )for m in pmatch.finditer(ast)]

            if len(lst_indexes) > 0:
                target = ast[:lst_indexes[0][0]]
                for i, ind in enumerate(lst_indexes):
                    start, end, w = ind
                    w = f"{w.split()[1]}"
                    if i == 0:
                        target = target + w
                    else:
                        target = target + ast[_end:start] + w
                    _end = end
                target = target + ast[lst_indexes[-1][1]:]
                
            return target
                
        for mod in self.modifiers:
            ast = process(mod, ast)

        return ast
    
    

    def inv_modifierpublic(self, ast):
        def process(mod, ast):
            pmatch = re.compile("modifier_{}".format(mod))
            target = ""
            for m in ast.split():
                if m == "modifier_{}".format(mod):
                    m = f"( modifier_{mod} ) modifier_{mod}"
                target += "{} ".format(m)
            return target
        for mod in self.modifiers:
            ast = process(mod, ast)
        return ast
    
    
    
    def blockstatement(self, ast):
        pmatch = re.compile("\( block \( expressionstatement \( methodinvocation_([^\s]*)")
        target = ast
        
        lst_indexes = [(m.start(), m.end(), m.group() )for m in pmatch.finditer(ast)]
        
        if len(lst_indexes) > 0:
            
            target = ast[:lst_indexes[0][0]]
            for i, ind in enumerate(lst_indexes):
                start, end, w = ind
                w = f"( block {w.split()[5]}"
                if i == 0:
                    target = target + w
                else:
                    target = target + ast[_end:start] + w
                _end = end
            target = target + ast[lst_indexes[-1][1]:]

        return target
    
    def inv_blockstatement(self, ast):
        pmatch = re.compile("\( block methodinvocation_([^\s]*)")
        target = ast
        
        
        lst_indexes = [(m.start(), m.end(), m.group() )for m in pmatch.finditer(ast)]
        
        if len(lst_indexes) > 0:
            
            target = ast[:lst_indexes[0][0]]
            for i, ind in enumerate(lst_indexes):
                start, end, w = ind
                w = f"( block ( expressionstatement ( {w.split()[2]}"
                if i == 0:
                    target = target + w
                else:
                    target = target + ast[_end:start] + w
                _end = end
            target = target + ast[lst_indexes[-1][1]:]

        return target
    

    def XLiteral(self, ast):
        def process(t, ast):
            pmatch = re.compile("\( {} \) {}".format(t, t))
            target = ast
            lst_indexes = [(m.start(), m.end(), m.group() )for m in pmatch.finditer(ast)]
            #print(lst_indexes)
            if len(lst_indexes) > 0:

                target = ast[:lst_indexes[0][0]]
                for i, ind in enumerate(lst_indexes):
                    start, end, w = ind
                    if w.split()[1] == w.split()[3] and w.split()[1] in self.ltypes and i == 0:
                        target = target + w.split()[1]
                    elif w.split()[1] == w.split()[3] and w.split()[1] in self.ltypes:
                        target = target + ast[_end:start] + w.split()[1]
                    _end = end
                target = target + ast[lst_indexes[-1][1]:]
            return target
        
        for _type in self.ltypes:
            ast = process(_type, ast)

        return ast
    
    def inv_XLiteral(self, ast):
        target = [f'( {s} ) {s}' if s in self.ltypes else s for s in ast.split()]
        target = ' '.join(target)
        return target

    def simplename(self, ast):
        pmatch = re.compile("\( simplename_([^\s]*) \) simplename_([^\s]*)")
        target = ast
        lst_indexes = [(m.start(), m.end(), m.group() )for m in pmatch.finditer(ast)]

        if len(lst_indexes) > 0:
            
            target = ast[:lst_indexes[0][0]]
            for i, ind in enumerate(lst_indexes):
                start, end, w = ind
                if i == 0:
                    target = target + w.split()[1]
                else:
                    target = target + ast[_end:start] + w.split()[1]
                _end = end
            target = target + ast[lst_indexes[-1][1]:]

        return target

    def inv_simplename(self, ast):
        pmatch = re.compile('simplename_([^\s]*)')

        target = ast
        lst_indexes = [(m.start(), m.end(), m.group() )for m in pmatch.finditer(ast)]

        if len(lst_indexes) > 0:
            target = ast[:lst_indexes[0][0]]
            for i, ind in enumerate(lst_indexes):
                start, end, w = ind
                w = f'( {w} ) {w}'
                if i == 0:
                    target = target + w
                else:
                    target = target + ast[_end:start] + w
                _end = end
            target = target + ast[lst_indexes[-1][1]:]

        return target


## In[2]:


#ast = "( MethodDeclaration ( SingleMemberAnnotation ( simplename_KnownFailure ) simplename_KnownFailure ( StringLiteral ) StringLiteral ) SingleMemberAnnotation ( Modifier_public ) Modifier_public ( primitivetype ) primitivetype ( simplename_test_unwrap_ByteBuffer$ByteBuffer_02 ) simplename_test_unwrap_ByteBuffer$ByteBuffer_02 ( block ( VariableDeclarationStatement ( simpletype ( simplename_String ) simplename_String ) simpletype ( VariableDeclarationFragment ( simplename_host ) simplename_host ( StringLiteral ) StringLiteral ) VariableDeclarationFragment ) VariableDeclarationStatement ( VariableDeclarationStatement ( primitivetype ) primitivetype ( VariableDeclarationFragment ( simplename_port ) simplename_port ( NumberLiteral ) NumberLiteral ) VariableDeclarationFragment ) VariableDeclarationStatement ( VariableDeclarationStatement ( simpletype ( simplename_ByteBuffer ) simplename_ByteBuffer ) simpletype ( VariableDeclarationFragment ( simplename_bbs ) simplename_bbs ( methodinvocation ( simplename_ByteBuffer ) simplename_ByteBuffer ( simplename_allocate ) simplename_allocate ( NumberLiteral ) NumberLiteral ) methodinvocation ) VariableDeclarationFragment ) VariableDeclarationStatement ( VariableDeclarationStatement ( simpletype ( simplename_ByteBuffer ) simplename_ByteBuffer ) simpletype ( VariableDeclarationFragment ( simplename_bbR ) simplename_bbR ( methodinvocation ( methodinvocation ( simplename_ByteBuffer ) simplename_ByteBuffer ( simplename_allocate ) simplename_allocate ( NumberLiteral ) NumberLiteral ) methodinvocation ( simplename_asReadOnlyBuffer ) simplename_asReadOnlyBuffer ) methodinvocation ) VariableDeclarationFragment ) VariableDeclarationStatement ( VariableDeclarationStatement ( ArrayType ( simpletype ( simplename_ByteBuffer ) simplename_ByteBuffer ) simpletype ) ArrayType ( VariableDeclarationFragment ( simplename_bbA ) simplename_bbA ( ArrayInitializer ( simplename_bbR ) simplename_bbR ( methodinvocation ( simplename_ByteBuffer ) simplename_ByteBuffer ( simplename_allocate ) simplename_allocate ( NumberLiteral ) NumberLiteral ) methodinvocation ( methodinvocation ( simplename_ByteBuffer ) simplename_ByteBuffer ( simplename_allocate ) simplename_allocate ( NumberLiteral ) NumberLiteral ) methodinvocation ) ArrayInitializer ) VariableDeclarationFragment ) VariableDeclarationStatement ( VariableDeclarationStatement ( simpletype ( simplename_SSLEngine ) simplename_SSLEngine ) simpletype ( VariableDeclarationFragment ( simplename_sse ) simplename_sse ( methodinvocation ( simplename_getEngine ) simplename_getEngine ( simplename_host ) simplename_host ( simplename_port ) simplename_port ) methodinvocation ) VariableDeclarationFragment ) VariableDeclarationStatement ( expressionstatement ( methodinvocation ( simplename_sse ) simplename_sse ( simplename_setUseClientMode ) simplename_setUseClientMode ( BooleanLiteral ) BooleanLiteral ) methodinvocation ) expressionstatement ( TryStatement ( block ( expressionstatement ( methodinvocation ( simplename_sse ) simplename_sse ( simplename_unwrap ) simplename_unwrap ( simplename_bbs ) simplename_bbs ( simplename_bbA ) simplename_bbA ) methodinvocation ) expressionstatement ( expressionstatement ( methodinvocation ( simplename_fail ) simplename_fail ( StringLiteral ) StringLiteral ) methodinvocation ) expressionstatement ) block ( CatchClause ( SingleVariableDeclaration ( simpletype ( simplename_ReadOnlyBufferException ) simplename_ReadOnlyBufferException ) simpletype ( simplename_iobe ) simplename_iobe ) SingleVariableDeclaration ( block ) block ) CatchClause ( CatchClause ( SingleVariableDeclaration ( simpletype ( simplename_Exception ) simplename_Exception ) simpletype ( simplename_e ) simplename_e ) SingleVariableDeclaration ( block ( expressionstatement ( methodinvocation ( simplename_fail ) simplename_fail ( InfixExpression ( simplename_e ) simplename_e ( StringLiteral ) StringLiteral ) InfixExpression ) methodinvocation ) expressionstatement ) block ) CatchClause ) TryStatement ) block ) MethodDeclaration"


## In[3]:


#cp = CastPipeline()
#cp.debug_ast_cast(ast.lower())
##print(cp.check_ast_cast(ast))


## In[4]:


#cast = cp.ast_to_cast(ast.lower())
#print(len(cast.split()))
#print(len(ast.split()))
#ast = cp.cast_to_ast(cast)
#print(len(ast.split()))
#print(len(cast.split()))


## In[ ]:





