import os
import sys

import communicator
import concode_model
import embed_model

from time import gmtime, strftime
from typing import List

model_path = "/app/models/semparsing.pt"
concode_path = "/app/models/model_acc_83.17_ppl_1.88_e10.pt"

# TODO set the correct value
concode_cpp_path = "/app/models/model_acc_83.17_ppl_1.88_e10.pt"


def _on_ast(text):
    print(f"backend _on_ast {text}",
          file=sys.stderr)
    if os.path.exists(model_path):
        res = ""
        if text is not None:
            res = embed_model.ast(text)
        return res
    else:
        return 1


def _on_jml(text):
    print(f"translation._on_jml {text}", file=sys.stderr)
    if os.path.exists(model_path):
        res = ""
        if text is not None:
            res = embed_model.jml(text)
        return res
    else:
        return 1


concode = concode_model.EmbedModels(model_path, concode_path)
concode_cpp = concode_model.EmbedModels(model_path, concode_cpp_path)


def _on_java(nl: List[str],
             var_names: List[str], var_types: List[str],
             method_names: List[str], method_returns: List[str]):
    print(f"translation._on_java {text}", file=sys.stderr)
    res = concode.translate(nl, var_names, var_types, method_names, method_returns)


def _on_cpp(text):
    print(f"translation._on_cpp {text}", file=sys.stderr)
    res = concode_cpp.translate(text)


def _on_acsl(text):
    print(f"backend _on_acsl {text}",
              file=sys.stderr)
    if os.path.exists(model_path):
        res = ""
        if text is not None:
            res = embed_model.acsl(text)
        return res
    else:
        return 1



# communicator.register_on_encode(_on_encode)
communicator.register_on_ast(_on_ast)
communicator.register_on_jml(_on_jml)
communicator.register_on_acsl(_on_acsl)
communicator.register_on_java(_on_java)
communicator.register_on_cpp(_on_cpp)
print("load model data", file=sys.stderr)

print(f"start loading at: {strftime('%a, %d %b %Y %H:%M:%S +0000', gmtime())}",
      file=sys.stderr)
embed_model.init(model_path)
print(f"stop loading at: {strftime('%a, %d %b %Y %H:%M:%S +0000', gmtime())}",
      file=sys.stderr)


# This call is blocking and must come last
print("start listening", file=sys.stderr)
communicator.start_listening()
