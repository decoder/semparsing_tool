#!/bin/bash

host="localhost"
port=700
# text="Find the maximum element of an array by searching in from the ends"
text="Getter for member f"
target_language="jml"

command="curl --request POST  --url http://$host:$port/semparser --header 'content-type: application/json'   --data '{\"text\": \"$text\", \"target_language\": \"$target_language\"}'"

cat <<EOF
command:
$command

response:
$(eval ${command})
EOF
