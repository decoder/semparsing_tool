#!/bin/bash

host="localhost"
port=700
text="X and Y must be within the interval [–200.0, 200]"
#target_language="acsl"
target_language="cpp"
command="curl --request POST  --url http://$host:$port/semparser --header 'content-type: application/json'   --data '{\"text\": \"$text\", \"target_language\": \"$target_language\"}'"

cat <<EOF
command:
$command

response:
$(eval ${command})
EOF
