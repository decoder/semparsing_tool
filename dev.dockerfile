FROM tiangolo/uwsgi-nginx-flask:python3.8

# --------------------
# Package installation
# --------------------

ARG DEBIAN_FRONTEND=noninteractive
ARG THREADS

# install SSL certificate of PKM server
COPY ./ssl /ssl
RUN apt-get clean && apt-get update && apt-get install -y -qq ca-certificates
RUN mkdir -p /usr/local/share/ca-certificates && chmod 755 /usr/local/share/ca-certificates && cp -f /ssl/pkm_docker.crt /usr/local/share/ca-certificates && chmod 644 /usr/local/share/ca-certificates/pkm_docker.crt && update-ca-certificates

RUN pip install --upgrade pip
COPY requirements.txt /
RUN pip install -r /requirements.txt
COPY ./api /app

ARG PUID
ARG GUID

# CREATE APP USER
RUN groupadd -g ${GUID} apiuser && \
useradd -r -u ${PUID} -g apiuser apiuser

RUN chown -R ${PUID}:${GUID} /app

